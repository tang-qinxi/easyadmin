<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace EasyAdmin\upload\driver;

use EasyAdmin\upload\FileBase;
use EasyAdmin\upload\driver\qnoss\Oss;
use EasyAdmin\upload\trigger\SaveDb;

/**
 * 七牛云上传
 * Class Qnoss
 * @package EasyAdmin\upload\driver
 */
class Qnoss extends FileBase
{

    /**
     * 重写上传方法
     * @return array|void
     */
    public function save($info=[])
    {
        parent::save();
        $upload = Oss::instance($this->uploadConfig)
            ->save($this->completeFilePath, $this->completeFilePath);
        if ($upload['save'] == true) {
            $info['upload_type']   = $this->uploadType;
            $info['original_name'] = $this->file->getOriginalName();
            $info['mime_type']     = $this->file->getOriginalMime();
            $info['file_ext']      = strtolower($this->file->getOriginalExtension());
            $info['url']           = $this->completeFileUrl;
            $info['create_time']   = time();
            SaveDb::trigger($this->tableName, $info);
        }
        $this->rmLocalSave();
        return $upload;
    }

}