define(["jquery", "easy-admin"], function ($, ea) {
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.project/index',
        add_url: 'system.project/add',
        edit_url: 'system.project/edit',
        delete_url: 'system.project/delete',
        export_url: 'system.project/export',
        modify_url: 'system.project/modify',
        configure_url: 'system.project/configure',
        configure_add_url: 'system.project/configure_add?p_id=',
        configure_edit_url: 'system.project/configure_edit',
        configure_delete_url: 'system.project/configure_del',
    };
    var configureinit = {
        table_elem: '#currentTables',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.project/index',
        add_url: 'system.project/add',
        edit_url: 'system.project/edit',
        delete_url: 'system.project/configure_del',
        export_url: 'system.project/export',
        modify_url: 'system.project/modify',
    };
    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID', search: false},
                    {field: 'sort', width: 80, title: '排序', edit: 'text', search: false},
                    {field: 'title', minWidth: 80, title: '项目名称'},
                    {field: 'logo', minWidth: 80, title: '项目图标', search: false, templet: ea.table.image},
                    {field: 'image', minWidth: 80, title: '项目图片', search: false, templet: ea.table.image},
                    {field: 'remark', minWidth: 80, title: '备注信息'},
                    {field: 'status', title: '状态', width: 85, search: 'select', selectList: {0: '禁用', 1: '启用'}, templet: ea.table.switch},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '查看参数',
                                url: init.configure_url,
                                method: 'open',
                                auth: 'configure',
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                                extend: 'data-full="true"',
                            }, {
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                            }]
                        ]
                    }
                ]],
            });
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        delete: function () {
            ea.listen();
        },
        configure: function () {
            var reg = new RegExp("(^|&)id=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            ea.table.render({
                init: configureinit,
                search: false,
                page: false,
                url: 'configure', //数据接口
                where:{id: r[2]},
                toolbar: [
                    'refresh',
                    [{
                        text: '添加',
                        url: init.configure_add_url+r[2],
                        method: 'open',
                        auth: 'configure_add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                    }],
                    'delete'
                ],
                cols: [[
                    {type: "checkbox"},
                    {field: 'sort', width: 80, title: '排序', edit: 'text'},
                    {
                        field: 'title',
                        minWidth: 80,
                        title: '配置名称',
                        templet: function (d) {
                            if (d.title === 'cost') {
                                return 'cost阶梯算法';
                            } else if (d.title === 'schedule_day') {
                                return '工期阶梯算法';
                            } else if (d.title === 'schedule') {
                                return '工期阶梯算法';
                            } else {
                                return d.title;
                            }
                        }
                    },
                    {
                        field: 'type',
                        minWidth: 80,
                        title: '录入方式',
                        templet: function (d) {
                            if (d.type === 4) {
                                return '下拉(累加)';
                            } else if (d.type === 2) {
                                return '填空';
                            } else if (d.type === 3) {
                                return '阶段';
                            } else if (d.type === 1) {
                                return '下拉(阶乘)';
                            } else if (d.type === 5) {
                                return ' 一切险专用';
                            } else {
                                return '未知';
                            }
                        }
                    },
                    {field: 'parameter', minWidth: 80, title: '参数项'},
                    {field: 'proportion', minWidth: 80, title: '计算比例%'},
                    {
                        field: 'required',
                        minWidth: 80,
                        title: '是否必填',
                        templet: function (d) {
                            if (d.required === 1) {
                                return '必填';
                            } else {
                                return '非必填';
                            }
                        }
                    },
                    {
                        field: 'is_count',
                        minWidth: 80,
                        title: '是否参与计算',
                        templet: function (d) {
                            if (d.is_count === 1) {
                                return '参与计算';
                            } else {
                                return '不参与计算';
                            }
                        }
                    },
                    {
                        field: 'status',
                        minWidth: 80,
                        title: '小程序是否显示',
                        templet: function (d) {
                            if (d.status === 1) {
                                return '显示';
                            } else {
                                return '隐藏';
                            }
                        }
                    },
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '编辑',
                                url: init.configure_edit_url,
                                method: 'open',
                                auth: 'configure_edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                            }, {
                                text: '删除',
                                method: 'confirm',
                                url: init.configure_delete_url,
                                auth: 'configure_delete',
                                class: 'layui-btn layui-btn-danger layui-btn-xs',
                            }]
                        ]
                    }
                ]],
            });
            ea.listen();
        },
        configure_add: function () {
            ea.listen();
        },
        configure_edit: function () {
            ea.listen();
        },
        configure_delete: function () {
            ea.listen();
        },
    };
    return Controller;
});

