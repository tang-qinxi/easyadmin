define(["jquery", "easy-admin"], function ($, ea) {
    var upload = layui.upload;
    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.record/index',
        edit_url: 'system.record/edit',
        export_url: 'system.record/export',
        material_url: 'system.record/material',
    };
    var materialinit = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.record/material',
        add_url: 'system.record/material_add?id=',
        delete_url: 'system.record/material_del',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', title: 'ID',search:false},
                    {
                        field: 'store_name',
                        minWidth: 100,
                        title: '关联门店',
                        templet: function (d) {
                            if (!d.store_name) {
                                return '平台';
                            }else{
                                return d.store_name;
                            }
                        }
                    },
                    {field: 'nickname', minWidth: 170, title: '业务员'},
                    {field: 'project_name', minWidth: 170, title: '险种'},
                    {field: 'project', minWidth: 100, title: '项目名称'},
                    {field: 'name', minWidth: 75, title: '联系人'},
                    {field: 'tel', minWidth: 120, title: '手机'},
                    {field: 'address', minWidth: 100, title: '项目地址',search:false},
                    {field: 'start', minWidth: 120, title: '项目开工时间',search:false},
                    {field: 'end', minWidth: 120, title: '项目完工时间',search:false},
                    {field: 'num', minWidth: 40, title: '份数',search:false},
                    {field: 'cost', minWidth: 120, title: '工程造价',search:false},
                    {field: 'price', minWidth: 120, title: '计算的价格',search:false},
                    {field: 'r_price', minWidth: 120, title: '核算的价格',search:false},
                    {field: 'status', title: '状态', width: 75, search: 'select', selectList: {0: '待跟进', 1: '跟进中',2: '已签订',3: '已放弃',4: '已丢单'}, templet: ea.table.select},
                    {field: 'admin_name', minWidth: 100, title: '跟进人'},
                    {field: 'remark', minWidth: 100, title: '备注',search:false},
                    {
                        width: 140,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '跟进',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                            },{
                                text: '资料管理',
                                url: init.material_url,
                                method: 'open',
                                auth: 'material',
                                class: 'layui-btn layui-btn-xs layui-btn-normal',
                                extend: 'data-full="true"',
                            }]
                        ]
                    },
                    {field: 'datas', minWidth: 100, title: '填写的内容',search:false},
                    {field: 'edit_time', minWidth: 160, title: '最后编辑时间', search: false},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: 'range'},
                    {field: 'company', width: 100, title: '公司名称'},
                    {field: 'business', minWidth: 120, title: '营业执照号码'},
                    {field: 'enterprise', minWidth: 100, title: '法人'},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        material: function () {
            var reg = new RegExp("(^|&)id=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            ea.table.render({
                init: materialinit,
                search: false,
                page: false,
                url: 'material', //数据接口
                where:{id: r[2]},
                toolbar: [
                    'refresh','upload','delete'
                ],
                cols: [[
                    {type: "checkbox"},
                    {field: 'store_name', minWidth: 100, title: '关联门店'},
                    {field: 'original_name', minWidth: 80, title: '文件名称'},
                    {
                        field: 'url',
                        minWidth: 80,
                        title: '文件地址',
                        templet: function (d) {
                            console.log(d)
                            if (d.url) {
                                return '<a href="'+d.url+'" target="_blank" title="下载 '+d.original_name+'" download="'+d.original_name+'"><i class="fa fa-download"></i> 下载文件</a>';
                            }
                        }
                    },
                    {field: 'file_ext', minWidth: 80, title: '文件类型'},
                    {field: 'file_size', minWidth: 80, title: '文件大小'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '删除',
                                method: 'confirm',
                                url: materialinit.delete_url,
                                auth: 'configure_delete',
                                class: 'layui-btn layui-btn-danger layui-btn-xs',
                            }]
                        ]
                    }
                ]],
            });
            ea.listen();
        },
        material_add: function () {
            ea.listen();
        },
    };

    setTimeout(function(){
        var reg = new RegExp("(^|&)id=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        upload.render({
            elem: "#btn-upload-a",
            url: '/admin/system.record/material_add?id='+r[2],
            exts: 'png|jpg|jpeg|xls|xlsx|doc|docx|ppt|pptx|zip|rar|7z',
            multiple: true,//是否多文件上传
            headers: ea.headers(),
            done: function (res) {
                if (res.code === 1) {
                    ea.msg.success(res.msg);
                    location.reload();
                } else {
                    ea.msg.error(res.msg);
                }
            },error: function(res){
                ea.msg.error(res.msg);
            }
        });
    },100)

    return Controller;
});