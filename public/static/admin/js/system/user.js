define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.user/index'
    };

    var projectinit = {
        table_elem: '#currentTables',
        table_render_id: 'currentTableRenderId',
        project_url: 'system.user/project',
    };

    var Controller = {
        index: function () {
            var util = layui.util;
            ea.table.render({
                elem: '#currentTable'
                ,url: 'index' //数据接口
                ,page: true //开启分页
                ,toolbar: ['refresh']
                // ,search:false
                // ,filter:false
                // ,exports:false
                ,cols: [[ //表头
                  // {field: 'avatar', minWidth: 80, title: '头像', search: false, templet: ea.table.image},
                  // {field: 'nickname', title: '昵称',},
                  {field: 'title', title: '关联门店',
                        templet: function (d) {
                            if (!d.title) {
                                return '平台';
                            }else{
                                return d.title;
                            }
                        }},
                  {field: 'tel', title: '手机号码', },
                  {field: 'login_count',  title: '登录次数',search: false},
                  {field: 'last_login_time',  title: '上次登录时间', width:200,search: false},
                  {field: 'create_time', title: '创建时间', width:200, search: 'range'},
                ]]
              });
            ea.listen();
        },
        project: function () {
            var util = layui.util;
            ea.table.render({
                init: projectinit,
                elem: '#currentTable'
                ,url: 'project' //数据接口
                ,page: true //开启分页
                ,toolbar: ['refresh']
                // ,search:false
                // ,filter:false
                // ,exports:false
                ,cols: [[ //表头
                  // {field: 'avatar', minWidth: 80, title: '头像', search: false, templet: ea.table.image},
                  // {field: 'nickname', minWidth: 200,title: '昵称',},
                  {field: 'store_name', minWidth: 200, title: '关联门店',
                        templet: function (d) {
                            if (!d.store_name) {
                                return '平台';
                            }else{
                                return d.store_name;
                            }
                        }},
                  {field: 'nickname', minWidth: 200, title: '业务员'},
                  {field: 'project_name', minWidth: 200, title: '险种'},
                  {field: 'company', width: 200, title: '公司名称'},
                  {field: 'project', minWidth: 200, title: '项目名称'},
                  {field: 'name', minWidth: 100, title: '联系人'},
                  {field: 'tel', minWidth: 200, title: '手机'},
                  {field: 'address', minWidth: 200, title: '项目地址'},
                  {field: 'start', minWidth: 100, title: '开工时间'},
                  {field: 'end', minWidth: 100, title: '完工时间'},
                  {field: 'business', minWidth: 200, title: '营业执照号码'},
                  {field: 'enterprise', minWidth: 100, title: '法人'},
                  {field: 'create_time', title: '创建时间', width:200, search: 'range'},
                ]]
              });
            ea.listen();
        }
    };
    return Controller;
});