define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.storeuser/index',
        add_url: 'system.storeuser/add',
        edit_url: 'system.storeuser/edit',
        delete_url: 'system.storeuser/delete',
        export_url: 'system.storeuser/export',
        modify_url: 'system.storeuser/modify',
    };


    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID',search:false},
                    {field: 'title', minWidth: 80, title: '门店名称'},
                    {field: 'nickname', minWidth: 80, title: '业务员姓名'},
                    {field: 'phone', minWidth: 80, title: '联系电话'},
                    {field: 'img_url', minWidth: 80, title: '二维码图', search: false, templet: ea.table.image},
                    {field: 'remark', minWidth: 80, title: '备注信息'},
                    {field: 'create_time', minWidth: 80, title: '创建时间', search: false},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            'edit',
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});