define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'article.lists/index',
        add_url: 'article.lists/add',
        edit_url: 'article.lists/edit',
        delete_url: 'article.lists/delete',
        export_url: 'article.lists/export',
        modify_url: 'article.lists/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh',
                    [{
                        text: '添加',
                        url: init.add_url,
                        method: 'open',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                        extend: 'data-full="true"',
                    }],
                    'delete', 'export'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 80, title: 'ID', search: false},
                    {field: 'sort', width: 80, title: '排序', edit: 'text', search: false},
                    {field: 'cate.title', minWidth: 120, title: '文章分类'},
                    {field: 'title', minWidth: 120, title: '文章名称'},
                    {field: 'images', minWidth: 120, title: '文章封面', search: false, templet: ea.table.image},
                    {field: 'describe', minWidth: 200, title: '文章描述', search: false},
                    {field: 'status', title: '状态', width: 95, selectList: {1: '启用',0: '禁用'},tips: '启用|禁用', templet: ea.table.switch},
                    {field: 'create_time', width: 180, title: '创建时间', search: false},
                    {
                        width: 150,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }],
                            'delete']
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        }
    };
    return Controller;
});