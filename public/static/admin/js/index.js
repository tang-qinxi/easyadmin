define(["jquery", "easy-admin", "echarts", "echarts-theme", "miniAdmin", "miniTab"], function ($, ea, echarts, undefined, miniAdmin, miniTab) {

    var laydate = layui.laydate;
    var Controller = {
        index: function () {
            var options = {
                iniUrl: ea.url('ajax/initAdmin'),    // 初始化接口
                clearUrl: ea.url("ajax/clearCache"), // 缓存清理接口
                urlHashLocation: true,      // 是否打开hash定位
                bgColorDefault: false,      // 主题默认配置
                multiModule: true,          // 是否开启多模块
                menuChildOpen: false,       // 是否默认展开菜单
                loadingTime: 0,             // 初始化加载时间
                pageAnim: true,             // iframe窗口动画
                maxTabNum: 20,              // 最大的tab打开数量
            };
            miniAdmin.render(options);

            $('.login-out').on("click", function () {
                ea.request.get({
                    url: 'login/out',
                    prefix: true,
                }, function (res) {
                    ea.msg.success(res.msg, function () {
                        window.location = ea.url('login/index');
                    })
                });
            });
        },
        welcome: function () {
            Controller.laodData();
            miniTab.listen();
            date = new Date().getFullYear();
            mon = new Date().getMonth()+1;
            laydate.render({
                elem: '#test8'
                ,type: 'month'
                ,range: '至'
                ,value: (date-1)+'-'+(mon+1)+' 至 '+ date+'-'+mon
                ,done: function(value){
                    Controller.laodData(value);
                }
            });

            /**
             * 查看公告信息
             **/
            $('body').on('click', '.layuimini-notice', function () {
                var title = $(this).children('.layuimini-notice-title').text(),
                    noticeTime = $(this).children('.layuimini-notice-extra').text(),
                    content = $(this).children('.layuimini-notice-content').html();
                var html = '<div style="padding:15px 20px; text-align:justify; line-height: 22px;border-bottom:1px solid #e2e2e2;background-color: #2f4056;color: #ffffff">\n' +
                    '<div style="text-align: center;margin-bottom: 20px;font-weight: bold;border-bottom:1px solid #718fb5;padding-bottom: 5px"><h4 class="text-danger">' + title + '</h4></div>\n' +
                    '<div style="font-size: 12px">' + content + '</div>\n' +
                    '</div>\n';
                layer.open({
                    type: 1,
                    title: '系统公告' + '<span style="float: right;right: 1px;font-size: 12px;color: #b1b3b9;margin-top: 1px">' + noticeTime + '</span>',
                    area: '300px;',
                    shade: 0.8,
                    id: 'layuimini-notice',
                    btn: ['查看', '取消'],
                    btnAlign: 'c',
                    moveType: 1,
                    content: html,
                    success: function (layero) {
                        var btn = layero.find('.layui-layer-btn');
                        btn.find('.layui-layer-btn0').attr({
                            href: 'https://gitee.com/zhongshaofa/layuimini',
                            target: '_blank'
                        });
                    }
                });
            });

        },
        editAdmin: function () {
            ea.listen();
        },
        editPassword: function () {
            ea.listen();
        },
        laodData: function (value) {
            $.ajax({
                url: ea.url('ajax/welcome'),
                type: "post",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                dataType: "json",
                headers:{'X-CSRF-TOKEN': window.CONFIG.CSRF_TOKEN},
                data: {
                    date: value
                },
                timeout: 60000,
                success: function (res) {
                    console.log(res);
                    $('#total_users').html(res.total_users);
                    $('#total_views').html(res.total_views);
                    $('#total_reckons').html(res.total_reckons);
                    $('#total_reckons_0').html(res.total_reckons_0);
                    $('#total_reckons_1').html(res.total_reckons_1);
                    $('#total_reckons_2').html(res.total_reckons_2);
                    $('#total_reckons_3').html(res.total_reckons_3);
                    $('#total_reckons_4').html(res.total_reckons_4);
                    var optionRecords = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        legend: {
                            data: ['用户数', '浏览量', '提交报价数', '待跟进', '跟进中', '已签订', '已放弃', '已丢单']
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        toolbox: {
                            feature: {
                                saveAsImage: {}
                            }
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: res.xdata,
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: res.ydata,
                    };
                    var echartsRecords = echarts.init(document.getElementById('echarts-records'), 'walden');
                    echartsRecords.setOption(optionRecords);
                    window.addEventListener("resize", function () {
                        echartsRecords.resize();
                    });
                }
            });
        }
    };
    return Controller;
});
