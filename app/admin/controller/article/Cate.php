<?php
namespace app\admin\controller\article;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\ArticleCate;
use app\admin\traits\Curd;
use app\common\controller\AdminController;
use think\App;

/**
 * Class Cate
 * @package app\admin\controller\article
 * @ControllerAnnotation(title="文章分类管理")
 */
class Cate extends AdminController
{
    use Curd;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new ArticleCate();
    }
}