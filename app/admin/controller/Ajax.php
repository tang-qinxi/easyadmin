<?php
namespace app\admin\controller;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\SystemUploadfile;
use app\common\controller\AdminController;
use app\common\service\MenuService;
use EasyAdmin\upload\Uploadfile;
use think\db\Query;
use think\facade\Cache;
use think\facade\Db;

class Ajax extends AdminController
{

    /**
     * 初始化后台接口地址
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function initAdmin()
    {
        // $cacheData = Cache::get('initAdmin_' . session('admin.id'));
        // if (!empty($cacheData)) {
        //     return json($cacheData);
        // }
        $menuService = new MenuService(session('admin.id'));
        $data = [
            'logoInfo' => [
                'title' => sysconfig('site', 'logo_title'),
                'image' => sysconfig('site', 'logo_image'),
                'href'  => __url('index/index'),
            ],
            'homeInfo' => $menuService->getHomeInfo(),
            'menuInfo' => $menuService->getMenuTree(),
        ];
        Cache::tag('initAdmin')->set('initAdmin_' . session('admin.id'), $data);
        return json($data);
    }

    /**
     * 清理缓存接口
     */
    public function clearCache()
    {
        Cache::clear();
        $this->success('清理缓存成功');
    }

    /**
     * 上传文件
     */
    public function upload()
    {
        $this->checkPostRequest();
        $data = [
            'upload_type' => $this->request->post('upload_type'),
            'file'        => $this->request->file('file'),
        ];
        $uploadConfig = sysconfig('upload');
        empty($data['upload_type']) && $data['upload_type'] = $uploadConfig['upload_type'];
        $rule = [
            'upload_type|指定上传类型有误' => "in:{$uploadConfig['upload_allow_type']}",
            'file|文件'              => "require|file|fileExt:{$uploadConfig['upload_allow_ext']}|fileSize:{$uploadConfig['upload_allow_size']}",
        ];
        $this->validate($data, $rule);
        try {
            $upload = Uploadfile::instance()
                ->setUploadType($data['upload_type'])
                ->setUploadConfig($uploadConfig)
                ->setFile($data['file'])
                ->save();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if ($upload['save'] == true) {
            $this->success($upload['msg'], ['url' => $upload['url']]);
        } else {
            $this->error($upload['msg']);
        }
    }

    /**
     * 上传图片至编辑器
     * @return \think\response\Json
     */
    public function uploadEditor()
    {
        $this->checkPostRequest();
        $data = [
            'upload_type' => $this->request->post('upload_type'),
            'file'        => $this->request->file('upload'),
        ];
        $uploadConfig = sysconfig('upload');
        empty($data['upload_type']) && $data['upload_type'] = $uploadConfig['upload_type'];
        $rule = [
            'upload_type|指定上传类型有误' => "in:{$uploadConfig['upload_allow_type']}",
            'file|文件'              => "require|file|fileExt:{$uploadConfig['upload_allow_ext']}|fileSize:{$uploadConfig['upload_allow_size']}",
        ];
        $this->validate($data, $rule);
        try {
            $upload = Uploadfile::instance()
                ->setUploadType($data['upload_type'])
                ->setUploadConfig($uploadConfig)
                ->setFile($data['file'])
                ->save();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if ($upload['save'] == true) {
            return json([
                'error'    => [
                    'message' => '上传成功',
                    'number'  => 201,
                ],
                'fileName' => '',
                'uploaded' => 1,
                'url'      => $upload['url'],
            ]);
        } else {
            $this->error($upload['msg']);
        }
    }

    /**
     * 获取上传文件列表
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getUploadFiles()
    {
        $get = $this->request->get();
        $page = isset($get['page']) && !empty($get['page']) ? $get['page'] : 1;
        $limit = isset($get['limit']) && !empty($get['limit']) ? $get['limit'] : 10;
        $title = isset($get['title']) && !empty($get['title']) ? $get['title'] : null;
        $this->model = new SystemUploadfile();
        $count = $this->model
            ->where(function (Query $query) use ($title) {
                !empty($title) && $query->where('original_name', 'like', "%{$title}%");
            })
            ->count();
        $list = $this->model
            ->where(function (Query $query) use ($title) {
                !empty($title) && $query->where('original_name', 'like', "%{$title}%");
            })
            ->page($page, $limit)
            ->order($this->sort)
            ->select();
        $data = [
            'code'  => 0,
            'msg'   => '',
            'count' => $count,
            'data'  => $list,
        ];
        return json($data);
    }

    /**
     * 后台欢迎页
     * @return string
     * @throws \Exception
     */
    public function welcome()
    {
        $store_id = session('admin.store_id');
        $map = [];
        if($store_id){
            $map['r.sid'] = $store_id;
        }
        $res['msg']             = '';
        $res['store_id']        = $store_id;
        $res['total_users']     = Db::name('user')->count();
        $res['total_views']     = Db::name('user_views')->alias('r')->where($map)->count();
        $res['total_reckons']   = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->count();
        $res['total_reckons_0'] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.status',0)->count();
        $res['total_reckons_1'] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.status',1)->count();
        $res['total_reckons_2'] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.status',2)->count();
        $res['total_reckons_3'] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.status',3)->count();
        $res['total_reckons_4'] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.status',4)->count();

        $endtime = date('Y-m',strtotime('last day of this month'));
        $starttime = date('Y-m', strtotime('first day of -11 month'));
        $date = $this->request->post('date','');
        if ($date) {
            $date_arr = explode(' 至 ', $date);
            $starttime = $date_arr[0];
            $endtime = $date_arr[1];
        }
        $xdata[] = $starttime;
        for ($i=1; $i < 12; $i++) {
            $thisMonth = date('Y-m', strtotime($starttime.' +'.$i.' month'));
            $xdata[] = $thisMonth;
            if($endtime==$thisMonth){
                break;
            }
        }
        $res['xdata'] = $xdata;

        foreach ($xdata as $ko => $vo) {
            $month_users[$ko]     = Db::name('user')->where('month',$vo)->count();
            $month_views[$ko]     = Db::name('user_views')->alias('r')->where($map)->where('month',$vo)->count();
            $month_reckons[$ko]   = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->count();
            $month_reckons_0[$ko] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->where('r.status',0)->count();
            $month_reckons_1[$ko] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->where('r.status',1)->count();
            $month_reckons_2[$ko] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->where('r.status',2)->count();
            $month_reckons_3[$ko] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->where('r.status',3)->count();
            $month_reckons_4[$ko] = Db::name('user_reckon')->alias('r')->where($map)->leftJoin('ea_user u','r.uid = u.id')->where('r.month',$vo)->where('r.status',4)->count();
        }

        $ydata[] = ['name'=>'用户数','type'=>'line','data'=>$month_users];
        $ydata[] = ['name'=>'浏览量','type'=>'line','data'=>$month_views];
        $ydata[] = ['name'=>'提交报价数','type'=>'line','data'=>$month_reckons];
        $ydata[] = ['name'=>'待跟进','type'=>'line','data'=>$month_reckons_0];
        $ydata[] = ['name'=>'跟进中','type'=>'line','data'=>$month_reckons_1];
        $ydata[] = ['name'=>'已签订','type'=>'line','data'=>$month_reckons_2];
        $ydata[] = ['name'=>'已放弃','type'=>'line','data'=>$month_reckons_3];
        $ydata[] = ['name'=>'已丢单','type'=>'line','data'=>$month_reckons_4];
        $res['ydata'] = $ydata;

        return $res;
    }
}