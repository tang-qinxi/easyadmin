<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\Store as StoreModel;
use app\admin\model\SystemAdmin;
use app\admin\traits\Curd;
use app\common\constants\AdminConstant;
use app\common\controller\AdminController;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Label\Margin\Margin;
use think\App;

/**
 * Class Store
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="门店管理")
 */
class Store extends AdminController
{
    protected $sort = [
        'sort' => 'desc',
        'id'   => 'desc',
    ];
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new StoreModel();
        $this->AdminModel = new SystemAdmin();
        $this->store_id = session('admin.store_id');
    }



    private function qr_code($store_id=0,$title='信息咨询与价格预报价')
    {
        $link = 'https://'.$_SERVER['HTTP_HOST'].'/?sid='.$store_id;
        $store_id = $store_id?$store_id:0;
        $qrcode_dir = public_path() . 'store/';
        if (!file_exists($qrcode_dir)) mkdir($qrcode_dir, 0777, true);
        $file_name = $qrcode_dir .$store_id . '.png';
        $file_url = 'https://'.$_SERVER['HTTP_HOST']. '/store/'.$store_id . '.png?v='.time();
        $writer = new PngWriter();
        $qrCode = QrCode::create($link)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(30)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));
        $label = Label::create($title)
            ->setFont(new NotoSans(20))
            ->setMargin(new Margin(0,0,20,0));
        $result = $writer->write($qrCode,null,$label);
        $result->saveToFile($file_name);
        return $file_url;
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                if($this->store_id){
                    $this->selectWhere = ['id'=>$this->store_id];
                }
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            foreach ($where as $k=>$v){
                if ($v[0]=='status'){
                    $where[$k][0] = 's.status';
                }
            }
            if($this->store_id){
                $where[] = [0=>'s.id',1=>'=',$this->store_id];
            }
            $count = $this->model
                ->alias('s')
                ->where($where)
                ->count();
            $list = $this->model
                ->alias('s')
                ->field('s.*,a.username')
                ->leftJoin('system_admin a','s.admin_id = a.id')
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add($id = null)
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'title|门店名称' => 'require',
                'nickname|联系人姓名'  => 'require',
                'phone|联系人电话'  => 'require',
                'username|登录账户'       => 'require',
                'password|登录密码'       => 'require',
                'password_again|确认密码' => 'require',
            ];
            $this->validate($post, $rule);
            if ($post['password'] != $post['password_again']) {
                $this->error('两次密码输入不一致');
            }
            unset($post['password_again']);
            $post['password'] = password($post['password']);
            $post['auth_ids'] = 2;
            try {
                $save = $this->AdminModel->save($post);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            if ($save) {
                $data = ['title'=>$post['title'],'remark'=>$post['remark'],'admin_id'=>$this->AdminModel->id];
                try {
                    $this->model->save($data);
                    $updata['id'] = $this->model->id;
                    $updata['img_url'] = $this->qr_code($this->model->id,$post['title']);
                    $this->model->save($updata);
                } catch (\Exception $e) {
                    $this->error('保存失败');
                }
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id)
    {
        $row = $this->model->find($id);
        empty($row) && $this->error('数据不存在');
        $admin_row = $this->AdminModel->find($row['admin_id']);
        $row['username'] = $admin_row['username'];
        $row['nickname'] = $admin_row['nickname'];
        $row['phone'] = $admin_row['phone'];
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'title|门店名称' => 'require',
                'nickname|联系人姓名'  => 'require',
                'phone|联系人电话'  => 'require',
                'username|登录账户'       => 'require',
            ];
            $this->validate($post, $rule);
            if(!empty($post['password'])||!empty($post['password_again'])){
                $rule = [
                    'password|登录密码'       => 'require',
                    'password_again|确认密码' => 'require',
                ];
                $this->validate($post, $rule);
                if ($post['password'] != $post['password_again']) {
                    $this->error('两次密码输入不一致');
                }
                $post['password'] = password($post['password']);
            }else{
                unset($row['password']);
                unset($row['password_again']);
            }
            try {
                $post['img_url'] = $this->qr_code($id,$post['title']);
                $save = $row->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            if ($save) {
                $post['auth_ids'] = 2;
                $admin_save = $admin_row->save($post);
            } else {
                $this->error('保存失败');
            }
            $admin_save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign([
            'id'          => $id,
            'row'         => $row,
        ]);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {
        $this->checkPostRequest();
        $row = $this->model->whereIn('id', $id)->select();
        empty($row) && $this->error('数据不存在');
        $aids = array_column($row->toArray(),'admin_id');
        $aids == AdminConstant::SUPER_ADMIN_ID && $this->error('超级管理员不允许修改');
        if (is_array($aids)){
            if (in_array(AdminConstant::SUPER_ADMIN_ID, $aids)){
                $this->error('超级管理员不允许修改');
            }
        }
        try {
            $save = $row->delete();
            $save_a = $this->AdminModel->whereIn('id', $aids)->save([
                    'delete_time' => time()
                ]);

        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        if ($save&&$save_a) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * @NodeAnotation(title="属性修改")
     */
    public function modify()
    {
        $this->checkPostRequest();
        $post = $this->request->post();
        $rule = [
            'id|ID'    => 'require',
            'field|字段' => 'require',
            'value|值'  => 'require',
        ];
        $this->validate($post, $rule);
        $row = $this->model->find($post['id']);
        if (!$row) {
            $this->error('数据不存在');
        }
        if (!in_array($post['field'], $this->allowModifyFields)) {
            $this->error('该字段不允许修改：' . $post['field']);
        }
        if ($row['admin_id'] == AdminConstant::SUPER_ADMIN_ID && $post['field'] == 'status') {
            $this->error('超级管理员状态不允许修改');
        }
        $admin_row = $this->AdminModel->find($row['admin_id']);
        empty($admin_row) && $this->error('数据不存在');
        
        try {
            $row->save([
                $post['field'] => $post['value'],
            ]);
            $admin_row->save([
                $post['field'] => $post['value'],
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('保存成功');
    }
}