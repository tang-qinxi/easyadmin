<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\SystemUploadfile;
use app\common\controller\AdminController;
use think\App;

/**
 * Class Uploadfile
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="上传文件管理")
 */
class Uploadfile extends AdminController
{
    use \app\admin\traits\Curd;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new SystemUploadfile();
    }
}