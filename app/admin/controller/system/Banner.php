<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\Banner as BannerModel;
use app\common\controller\AdminController;
use app\admin\traits\Curd;
use think\App;

/**
 * Class Banner
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="广告管理")
 */
class Banner extends AdminController
{

    use Curd;
    protected $sort = [
        'sort' => 'desc',
        'id'   => 'desc',
    ];
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new BannerModel();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }
}