<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\Storeuser as StoreuserModel;
use app\admin\model\Store as StoreModel;
use app\admin\traits\Curd;
use app\common\constants\AdminConstant;
use app\common\controller\AdminController;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Label\Margin\Margin;
use think\App;

/**
 * Class Store
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="业务员管理")
 */
class Storeuser extends AdminController
{
    protected $sort = [
        'sort' => 'desc',
        'id'   => 'desc',
    ];
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new StoreuserModel();
        $this->StoreModel = new StoreModel();
        $this->store_id = session('admin.store_id');
    }

    private function qr_code($store_id=0,$uid=0,$title='信息咨询与价格预报价')
    {
        $link = 'https://'.$_SERVER['HTTP_HOST'].'/?sid='.$store_id.'&cuid='.$uid;
        $qrcode_dir = public_path() . 'storeuser/';
        if (!file_exists($qrcode_dir)) mkdir($qrcode_dir, 0777, true);
        $file_name = $qrcode_dir .$store_id.'_'.$uid . '.png';
        $file_url = 'https://'.$_SERVER['HTTP_HOST']. '/storeuser/'.$store_id.'_'.$uid.'.png?v='.time();
        $writer = new PngWriter();
        $qrCode = QrCode::create($link)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(30)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));
        $label = Label::create($title)
            ->setFont(new NotoSans(20))
            ->setMargin(new Margin(0,0,20,0));
        $result = $writer->write($qrCode,null,$label);
        $result->saveToFile($file_name);
        return $file_url;
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            foreach ($where as $k=>$v){
                if ($v[0]=='status'){
                    $where[$k][0] = 'u.status';
                }
            }
            if($this->store_id){
                $where[] = [0=>'u.store_id',1=>'=',$this->store_id];
            }
            $count = $this->model
                ->alias('u')
                ->where($where)
                ->count();
            $list = $this->model
                ->alias('u')
                ->field('u.*,s.title')
                ->leftJoin('store s','u.store_id = s.id')
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="添加")
     */
    public function add($id = null)
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'store_id|门店' => 'require',
                'nickname|业务员姓名'  => 'require',
                'phone|联系电话'  => 'require',
            ];

            if($this->store_id){
                $post['store_id'] = $this->store_id;
            }

            $this->validate($post, $rule);
            $store = $this->StoreModel->find($post['store_id']);
            if ($store) {
                try {
                    $this->model->save($post);
                    $updata['id'] = $this->model->id;
                    $updata['img_url'] = $this->qr_code($post['store_id'],$this->model->id,$store['title']);
                    $this->model->save($updata);
                } catch (\Exception $e) {
                    $this->error('保存失败');
                }
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id)
    {
        $where = ['id'=>$id];
        if($this->store_id){
            $where['store_id'] = $this->store_id;
        }

        $row = $this->model->where($where)->find();
        empty($row) && $this->error('数据不存在');
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'store_id|门店' => 'require',
                'nickname|业务员姓名'  => 'require',
                'phone|联系电话'  => 'require',
            ];
            $this->validate($post, $rule);

            if($this->store_id){
                $post['store_id'] = $this->store_id;
            }

            $store = $this->StoreModel->find($post['store_id']);
            if ($store) {
                try {
                    $post['img_url'] = $this->qr_code($post['store_id'],$id,$store['title']);
                    $save = $row->save($post);
                } catch (\Exception $e) {
                    $this->error('保存失败');
                }
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign([
            'id'          => $id,
            'row'         => $row,
        ]);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function delete($id)
    {
        $this->checkPostRequest();

        $where[] = ['id','in',$id];
        if($this->store_id){
            $where[] =['store_id','=',$this->store_id];
        }
        $row = $this->model->where($where)->select();
        empty($row) && $this->error('数据不存在');
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        if ($save) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

}