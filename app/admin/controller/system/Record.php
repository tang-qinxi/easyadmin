<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\UserReckon as RecordModel;
use app\admin\model\UserReckonUploadfile as FileModel;
use EasyAdmin\upload\Uploadfile;
use app\common\controller\AdminController;
use think\App;

/**
 * Class Record
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="填报记录")
 */
class Record extends AdminController
{
    protected $sort = [
        'sort' => 'desc',
        'id'   => 'desc',
    ];
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new RecordModel();
        $this->FileModel = new FileModel();
        $this->store_id = session('admin.store_id');
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            foreach ($where as $k=>$v){
                $where[$k][0] = 'r.'.$v[0];
                if ($v[0]=='store_name'){
                    $where[$k][0] = 's.title';
                    if($v[2]=="%平台%"){
                        $where[$k][0] = 'r.sid';
                        $where[$k][1] = '=';
                        $where[$k][2] = '0';
                    }
                }
                if ($v[0]=='project_name'){
                    $where[$k][0] = 'p.title';
                }
                if ($v[0]=='nickname'){
                    $where[$k][0] = 'su.nickname';
                }
            }

            if($this->store_id){
                $where[] = [0=>'r.sid',1=>'=',$this->store_id];
            }

            $count = $this->model
                ->alias('r')
                ->field('r.id')
                ->leftJoin('ea_user u','r.uid = u.id')
                ->leftJoin('ea_store s','r.sid = s.id')
                ->leftJoin('ea_project p','r.pid = p.id')
                ->leftJoin('ea_storeuser su','r.cuid = su.id')
                ->where($where)
                ->order('r.id desc')
                ->count();
            $list = $this->model
                ->alias('r')
                ->field('r.*,s.title as store_name,p.title as project_name,su.nickname')
                ->leftJoin('ea_user u','r.uid = u.id')
                ->leftJoin('ea_store s','r.sid = s.id')
                ->leftJoin('ea_storeuser su','r.cuid = su.id')
                ->leftJoin('ea_project p','r.pid = p.id')
                ->where($where)
                ->order('r.id desc')
                ->page($page, $limit)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit($id)
    {
        $row = $this->model->find($id);
        empty($row) && $this->error('数据不存在');
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'status|跟进状态'   => 'require',
                'r_price|核算的价格'   => 'require',
                'remark|备注'    => 'require',
            ];
            $this->validate($post, $rule);
            $post['admin_id'] = session('admin.id');
            $post['admin_name'] = session('admin.username');
            $post['edit_time'] = date('Y-m-d H:i:s');
            // $post['remark'] = $row['remark'].PHP_EOL.$post['remark'];
            try {
                $save = $row->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign([
            'id'          => $id,
            'row'         => $row,
        ]);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="资料管理")
     */
    public function material($id = null)
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            if (!$id) {
                $this->error('数据获取失败！');
            }
            $list = $this->FileModel
                ->alias('f')
                ->field('f.*,s.title as store_name')
                ->leftJoin('ea_store s','f.sid = s.id')
                ->where('rid',$id)
                ->order('f.id desc')
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="资料上传")
     */
    public function material_add($id = null)
    {
        $this->checkPostRequest();
        $data = [
            'upload_type' => $this->request->post('upload_type'),
            'file'        => $this->request->file('file'),
        ];
        $uploadConfig = sysconfig('upload');
        empty($data['upload_type']) && $data['upload_type'] = $uploadConfig['upload_type'];
        $rule = [
            'upload_type|指定上传类型有误' => "in:{$uploadConfig['upload_allow_type']}",
            'file|文件'              => "require|file|fileExt:{$uploadConfig['upload_allow_ext']}|fileSize:{$uploadConfig['upload_allow_size']}",
        ];
        $this->validate($data, $rule);
        try {
            $size = round($data['file']->getsize()/1048576,2).'M';
            $upload = Uploadfile::instance()
                ->setUploadType($data['upload_type'])
                ->setUploadConfig($uploadConfig)
                ->setTableName('user_reckon_uploadfile')
                ->setFile($data['file'])
                ->save(['rid'=>$id,'admin_id'=>session('admin.id'),'admin_name'=>session('admin.username'),'sid'=>session('admin.store_id'),'file_size'=>$size]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        if ($upload == true) {
            $this->success($upload['msg'], ['url' => $upload['url']]);
        } else {
            $this->error($upload['msg']);
        }
    }

    /**
     * @NodeAnotation(title="资料删除")
     */
    public function material_del($id)
    {
        $this->checkPostRequest();
        $row = $this->FileModel->whereIn('id', $id)->select();
        empty($row) && $this->error('数据不存在');
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }
}