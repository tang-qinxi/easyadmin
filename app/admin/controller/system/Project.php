<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\Project as projectModel;
use app\admin\model\ProjectConfigure as configureModel;
use app\admin\traits\Curd;
use app\common\controller\AdminController;
use think\App;

/**
 * Class Project
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="项目管理")
 */
class Project extends AdminController
{
    use Curd;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new projectModel();
        $this->configureModel = new configureModel();
    }

    /**
     * @NodeAnotation(title="项目参数")
     */
    public function configure($id = null)
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            if (!$id) {
                $this->error('数据获取失败！');
            }
            $list = $this->configureModel
                ->where('p_id',$id)
                ->order('sort desc')
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="项目参数添加")
     */
    public function configure_add($p_id = null)
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if (!$p_id) {
                $this->error('保存失败');
            }
            $post['p_id'] = $p_id;
            if($post['type']==1){
                $rule = [
                    'title|配置名称'   => 'require',
                    'type|录入方式'    => 'require',
                    'parameter|参数项' => 'require',
                ];
                $post['proportion'] = 0;
            }else if($post['type']==2){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                ];
                $post['parameter'] = [];
            }else if($post['type']==3){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                    'parameter|参数项'    => 'require',
                ];
            }else if($post['type']==4){
                $rule = [
                    'title|配置名称'   => 'require',
                    'type|录入方式'    => 'require',
                    'parameter|参数项' => 'require',
                ];
                $post['proportion'] = 0;
            }else if($post['type']==5){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                    'parameter|参数项'    => 'require',
                ];
            }else{
                $this->error('参数错误');
            }
            $this->validate($post, $rule);
            if($post['type']==1||$post['type']==3){
                $parameter = explode(PHP_EOL,$post['parameter']);
                $parameter = array_values(array_unique(array_filter($parameter)));
                $post['parameter'] = implode(PHP_EOL,$parameter);
                $post['parameter'] = html_entity_decode($post['parameter']);
            }
            try {
                $save = $this->configureModel->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="项目参数编辑")
     */
    public function configure_edit($id)
    {
        $row = $this->configureModel->find($id);
        empty($row) && $this->error('数据不存在');
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if($post['type']==1){
                $rule = [
                    'title|配置名称'   => 'require',
                    'type|录入方式'    => 'require',
                    'parameter|参数项' => 'require',
                ];
                $post['proportion'] = 0;
            }else if($post['type']==2){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                ];
                $post['parameter'] = [];
            }else if($post['type']==3){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                    'parameter|参数项'    => 'require',
                ];
            }else if($post['type']==4){
                $rule = [
                    'title|配置名称'   => 'require',
                    'type|录入方式'    => 'require',
                    'parameter|参数项' => 'require',
                ];
                $post['proportion'] = 0;
            }else if($post['type']==5){
                $rule = [
                    'title|配置名称'      => 'require',
                    'type|录入方式'       => 'require',
                    'proportion|计算比例' => 'require',
                    'parameter|参数项'    => 'require',
                ];
            }else{
                $this->error('参数错误');
            }
            $this->validate($post, $rule);
            if($post['type']==1||$post['type']==3){
                $parameter = explode(PHP_EOL,$post['parameter']);
                $parameter = array_values(array_unique(array_filter($parameter)));
                $post['parameter'] = implode(PHP_EOL,$parameter);
                $post['parameter'] = html_entity_decode($post['parameter']);
            }
            try {
                $save = $row->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign([
            'id'          => $id,
            'row'         => $row,
        ]);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="项目参数删除")
     */
    public function configure_del($id)
    {
        $this->checkPostRequest();
        $row = $this->configureModel->whereIn('id', $id)->select();
        empty($row) && $this->error('数据不存在');
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }
}