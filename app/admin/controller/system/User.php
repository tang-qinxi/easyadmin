<?php
namespace app\admin\controller\system;

use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use app\admin\model\User as UserModel;
use app\admin\model\UserProject as UserProjectModel;
use app\common\controller\AdminController;
use think\App;

/**
 * Class User
 * @package app\admin\controller\system
 * @ControllerAnnotation(title="用户管理")
 */
class User extends AdminController
{
    protected $sort = [
        'sort' => 'desc',
        'id'   => 'desc',
    ];
    protected $store_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->model = new UserModel();
        $this->store_id = session('admin.store_id');
    }

    /**
     * @NodeAnotation(title="用户列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->alias('u')
                ->where($where)
                ->order('u.id desc')
                ->count();
            $list = $this->model
                ->alias('u')
                ->where($where)
                ->order('u.id desc')
                ->page($page, $limit)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="项目列表")
     */
    public function project()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            $this->ProjectModel = new UserProjectModel();
            list($page, $limit, $where) = $this->buildTableParames();
            foreach ($where as $k=>$v){
                if ($v[0]=='status'){
                    $where[$k][0] = 'up.status';
                }
                if ($v[0]=='store_name'){
                    $where[$k][0] = 's.title';
                    if($v[2]=="%平台%"){
                        $where[$k][0] = 'up.sid';
                        $where[$k][1] = '=';
                        $where[$k][2] = '0';
                    }
                }
                if ($v[0]=='project_name'){
                    $where[$k][0] = 'p.title';
                }
                if ($v[0]=='nickname'){
                    $where[$k][0] = 'su.nickname';
                }
            }
            
            if($this->store_id){
                $where[] = [0=>'up.sid',1=>'=',$this->store_id];
            }

            $count = $this->ProjectModel
                ->alias('up')
                ->field('up.id')
                ->leftJoin('ea_user u','up.uid = u.id')
                ->leftJoin('ea_store s','up.sid = s.id')
                ->leftJoin('ea_storeuser su','up.cuid = su.id')
                ->leftJoin('ea_project p','up.pid = p.id')
                ->where($where)
                ->count();
            $list = $this->ProjectModel
                ->alias('up')
                ->field('up.*,s.title as store_name,p.title as project_name,su.nickname')
                ->leftJoin('ea_user u','up.uid = u.id')
                ->leftJoin('ea_store s','up.sid = s.id')
                ->leftJoin('ea_storeuser su','up.cuid = su.id')
                ->leftJoin('ea_project p','up.pid = p.id')
                ->where($where)
                ->order('up.id desc')
                ->page($page, $limit)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }
}