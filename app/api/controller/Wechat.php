<?php
/**
 * Created by Aliz
 * User: 646594159@qq.com
 * Date: 2021/7/20
 * Time: 10:00
 */
namespace app\api\controller;
use app\BaseController;
use app\admin\model\User;
use app\admin\model\UserProject;
use app\admin\model\UserReckon;
use app\admin\model\Project;
use app\admin\model\ProjectConfigure;
use think\facade\Cache;
use think\facade\Env;
use think\facade\Db;
use EasyAdmin\tool\CommonTool;

class Wechat extends BaseController
{
    protected $userInfo;
    protected $sid;
    protected $cuid;
    protected function initialize()
    {
        $token = $this->request->param('token/s','');
        $this->sid = $this->request->param('sid/d',0);
        $this->cuid = $this->request->param('cuid/d',0);
        $this->UserModel = new User();
        $this->checkLogin($token);
        $data = [
            'sid'         => $this->sid,
            'cuid'        => $this->cuid,
            'uid'         => $this->userInfo['id'],
            'url'         => $this->request->url(),
            'title'       => '',
            'ip'          => CommonTool::getRealIp(),
            'month'       => date('Y-m'),
            'create_time' => time(),
        ];
        Db::name('user_views')->insert($data);
    }

    /**
     * access_token
     **/
    protected function getAccessToken(){
        $access_token = cache('access_token');
        if ($access_token) {
            return $access_token;
        }else{
            $appid = Env::get('WECHAT.APPID', '');
            $appsecrt = Env::get('WECHAT.APPSECRT', '');
            $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$appsecrt;
            $data = https_request($url);
            $data = json_decode($data,true);
            if(!empty($data['access_token'])){
                Cache::set('access_token', $data['access_token'], 7000);
                return $data['access_token'];
            }else{
                echo  json_encode(["code"=>"0","message"=>"获取到的凭证失效，请重试！"]);exit();
            }
        }
    }

    /**
     * 判断用户是否登录
     **/
    protected function checkLogin($token = ''){
        if(empty($token)){
            echo  json_encode(["code"=>"99","message"=>"登录失效，请重新登录"]);exit();
        }
        $this->userInfo = $this->UserModel->where(['token'=>$token])->find();
        if(!$this->userInfo){
            echo  json_encode(["code"=>"99","message"=>"登录失效，请重新登录"]);exit();
        }
    }

    /**
     * 手机号
     **/
    public function getPhoneNumber(){
        if ($this->request->isPost()) {
            $code=$this->request->param('code');
            $access_token=$this->getAccessToken();
            $url = 'https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token='.$access_token; //客户号
            $result=https_request($url,json_encode(['code'=>$code]));
            $arr = json_decode($result,true);
            $tel = '';
            if($arr['errcode']==0&&$arr['phone_info']){
                $tel = $arr['phone_info']['phoneNumber'];
            }else{
                return json(['code'=>'0','message'=>"保存失败"]);
            }
            try{
                $data = array();
                $data['update_time'] = time();
                $data['tel'] = $tel;
                $save = $this->UserModel->where('id',$this->userInfo['id'])->save($data);
                $info = $this->UserModel->where('id',$this->userInfo['id'])->find();
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            if ($save) {
                return json(['code'=>'1','message'=>"保存成功",'result'=>['token'=>$info['token'],'tel'=>$info['tel']]]);
            }else{
                return json(['code'=>'0','message'=>"保存失败"]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 项目详情
     **/
    public function projectInfo(){
        if ($this->request->isPost()) {
            $this->model = new project();
            $this->UPmodel = new UserProject();
            $this->configureModel = new ProjectConfigure();
            $id = $this->request->param('id/d','');
            try{
                $info = $this->model->field('id,title,type,image,remark,describe')->where('status', 1)->where('id', $id)->find();
                if (empty($info)) {
                    return json(['code'=>'0','message'=>'项目不存在']);
                }
                $info['describe'] = html_entity_decode($info['describe']);
                $info['have_info'] = $this->UPmodel->where('pid',$id)->where('uid', $this->userInfo['id'])->count();
                $info['item'] = $this->UPmodel->field('id,project,province,city,assured as enterprise,schedule,schedule_day')->where('pid',$id)->where('uid', $this->userInfo['id'])->order('id desc')->find();
                $configure = $this->configureModel->field('id,type,title,proportion,parameter,required,relevancy')->where('p_id',$id)->where('status', 1)->order('sort', 'desc')->select();
                foreach ($configure as $ko => $vo) {
                    if ($vo['type']==1||$vo['type']==4) {
                        $parameter = explode(PHP_EOL,$vo['parameter']);
                        foreach ($parameter as $k => $v) {
                            $_parameter = explode('|',$v);
                            $parameter[$k] = $_parameter[0];
                        }
                        $configure[$ko]['parameter'] = $parameter;
                    }elseif ($vo['type']==5) {
                        $_parameter = [];
                        $parameter = explode(PHP_EOL,$vo['parameter']);
                        foreach ($parameter as $v) {
                            $_parameter[] = ['key' => $v,'value' => ''];
                        }
                        $configure[$ko]['parameter'] = $_parameter;
                    }else{
                        $configure[$ko]['parameter'] = [];
                    }
                }
                $info['configure'] = $configure;


            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的信息列表
     **/
    public function company(){
        if ($this->request->isPost()) {
            $this->model = new UserProject();
            $pid = $this->request->param('pid/d',0);
            try{
                $list = $this->model->field('id,project,province,city,assured as enterprise,schedule,schedule_day')->where('uid', $this->userInfo['id'])->where('pid', $pid)->order('id', 'desc')->select();
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的信息详情
     **/
    public function companyInfo(){
        if ($this->request->isPost()) {
            $id = $this->request->param('id/d','');
            $this->model = new UserProject();
            try{
                $info = $this->model->field('*')->where('uid', $this->userInfo['id'])->where('id', $id)->find();
                if($info['pid']==6){
                    $info['cost']=(int)$info['cost'];
                }
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的信息详情
     **/
    public function companyAdd()
    {
        if (!$this->request->isPost()) {
            return json(['code'=>'0','message'=>"当前请求不合法！"]);
        }
        $this->model = new UserProject();
        $post = $this->request->post();
        $rule = [
            'pid|项目ID' => 'require',
            'applicant|投保人' => 'require',
            'assured|被保险人' => 'require',
            'name|联系人' => 'require',
            'tel|联系电话' => 'require',
            'province|项目省份' => 'require',
            'city|项目地级市' => 'require',
            'address|项目详细地址' => 'require',
        ];
        $start = new \DateTime($post['start']);
        $post['end'] = date("Y-m-d", strtotime($post['start']." +".$post['schedule_day']." days"));
        $end = new \DateTime($post['end']);
        $interval  = $start->diff($end);
        $y = $interval->y;
        $m = $interval->m;
        $d = $interval->d;
        if($d){
            $m ++;
        }
        if($post['type']==2){
            if($post['schedule']==0){
                return json(['code'=>'0','message'=>'请输入投保期限(年)']);
            }
        }else{
            if($post['project']==''){
                return json(['code'=>'0','message'=>'请输入工程名称']);
            }
            if($post['schedule_day']==0){
                return json(['code'=>'0','message'=>'请输入预计工期(天)']);
            }
            $post['schedule'] = $m+$y*12;
            if($post['schedule']>60&&$post['pid']==5){
                return json(['code'=>'0','message'=>'投保期限不能超过5年']);
            }
        }

        try {
            parent::validate($post, $rule);
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }

        $post['uid'] = $this->userInfo['id'];
        $post['sid'] = $this->sid;
        $post['cuid'] = $this->cuid;
        try {
            $save = $this->model->save($post);
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }

        if ($save) {
            $data=[
                'id'=>$this->model->id,
                'project'=>$post['project'],
                'province'=>$post['province'],
                'city'=>$post['city'],
                'enterprise'=>$post['assured'],
                'schedule'=>$post['schedule'],
            ];
            return json(['code'=>'1','message'=>"保存成功",'result'=>$data]);
        }else{
            return json(['code'=>'0','message'=>"保存失败"]);
        }
    }

    /**
     * 编辑我的信息
     **/
    public function companyEdit($id)
    {
        if (!$this->request->isPost()) {
            return json(['code'=>'0','message'=>"当前请求不合法！"]);
        }
        $this->model = new UserProject();
        $row = $this->model->where('uid', $this->userInfo['id'])->find($id);
        if (empty($row)) {
            return json(['code'=>'0','message'=>"数据不存在"]);
        }
        $post = $this->request->post();
        $rule = [
            'pid|项目ID' => 'require',
            'applicant|投保人' => 'require',
            'assured|被保险人' => 'require',
            'name|联系人' => 'require',
            'tel|联系电话' => 'require',
            'province|项目省份' => 'require',
            'city|项目地级市' => 'require',
            'address|项目详细地址' => 'require',
        ];

        $start = new \DateTime($post['start']);
        $post['end'] = date("Y-m-d", strtotime($post['start']." +".$post['schedule_day']." days"));
        $end = new \DateTime($post['end']);
        $interval  = $start->diff($end);
        $y = $interval->y;
        $m = $interval->m;
        $d = $interval->d;
        if($d){
            $m ++;
        }
        if($post['type']==2){
            if($post['schedule']==0){
                return json(['code'=>'0','message'=>'请输入投保期限(年)']);
            }
        }else{
            if($post['project']==''){
                return json(['code'=>'0','message'=>'请输入工程名称']);
            }
            $post['schedule'] = $m+$y*12;
            if($post['schedule_day']==0){
                return json(['code'=>'0','message'=>'请输入预计工期(天)']);
            }
            if($post['schedule']>60&&$post['pid']==5){
                return json(['code'=>'0','message'=>'投保期限不能超过5年']);
            }
        }

        try {
            parent::validate($post, $rule);
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }

        try {
            $post['delete_time'] = $row['delete_time'];
            $post['sid'] = $this->sid;
            $post['cuid'] = $this->cuid;
            $save = $row->save($post);
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }
        if ($save) {
            return json(['code'=>'1','message'=>"保存成功",'result'=>[]]);
        }else{
            return json(['code'=>'0','message'=>"保存失败"]);
        }
    }

    /**
     * 删除我的信息
     **/
    public function companyDel($id)
    {
        if (!$this->request->isPost()) {
            return json(['code'=>'0','message'=>"当前请求不合法！"]);
        }
        $this->model = new UserProject();
        $row = $this->model->whereIn('id', $id)->where('uid', $this->userInfo['id'])->select();
        if (empty($row)) {
            return json(['code'=>'0','message'=>"数据不存在"]);
        }
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }
        if ($save) {
            return json(['code'=>'1','message'=>"删除成功",'result'=>[]]);
        }else{
            return json(['code'=>'0','message'=>"删除失败"]);
        }
    }

    /**
     * 计算价格
     **/
    protected function getPrice($configure,$_configure,$pid,$UserProject){
        $price = 0;
        switch ($pid) {
            case 1:
                // 1   工程一切险 ?
                // 附加险费用
                foreach ($_configure as $vo) {
                    foreach ($configure as $v) {
                        if($v['is_count']==1){ // 参与计算
                            if($vo['id']==$v['id']){
                                if($v['type']==5){
                                    $coverage = 1;
                                    foreach ($vo['value'] as $vv){
                                        $coverage = bcmul($coverage,$vv['value'],20);
                                    }
                                    $coverage = bcdiv(bcmul($coverage,$v['proportion'],20),100,20);
                                    $price = bcadd($price,$coverage,20);
                                }
                            }
                        }
                    }
                }
                break;
            case 2:
                // 2   旅行社旅游质保金保险 ok
                $price = 1;
                foreach ($_configure as $vo) {
                    foreach ($configure as $v) {
                        if($vo['id']==$v['id']&&$v['is_count']==1){
                            if($v['type']==2){
                                $_price = $price;
                                $price = bcdiv(bcmul($vo['value'],$v['proportion'],20),100,20);
                                $price = bcmul($_price,$price,20);
                            }
                        }
                    }
                }
                break;
            case 3:
                // 3   建工意外险（总承包）ok
                $coverage = 0;
                $_coverage = 1;
                $price = 1;
                foreach ($_configure as $vo) {

                    if($vo['id']=='11'){
                        $tips = $vo['value'];
                    }
                    foreach ($configure as $v) {
                        if($v['is_count']==1){
                            if($vo['id']==$v['id']&&$vo['value']!='无'){
                                if($v['type']==4){
                                    // 相加
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $coverage = bcadd($coverage,$par[1],20);
                                        }
                                    }
                                }else if($v['type']==1){
                                    // 相乘
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            if($vo['id']==49){
                                                $_coverage = bcdiv(bcmul($_coverage,$par[1],20),100,20);
                                            }
                                            $price = bcdiv(bcmul($price,$par[1],20),100,20);
                                        }
                                    }
                                }else if($v['type']==2){
                                    // 填空
                                    $price = bcmul($price,$vo['value'],20);
                                }
                            }
                        }
                    }
                }
                if ($tips=='造价') {
                    $price = bcmul($coverage,$UserProject['cost'],20);
                    $price = bcmul($price,$_coverage,20);
                }
                break;
            case 4:
                // 4   农民工工资保证保险 确认ok
                $coverage = 0;
                foreach ($_configure as $vo) {
                    foreach ($configure as $v) {
                        if($vo['id']==$v['id']&&$v['is_count']==1){
                            if($v['type']==1){
                                $parameter=explode(PHP_EOL,$v['parameter']);
                                foreach ($parameter as $value) {
                                    $par=explode('|',$value);
                                    if ($par[0]==$vo['value']) {
                                        $coverage = bcdiv(bcmul($UserProject['cost'],$par[1],20),100,20);
                                    }
                                }
                            }
                        }
                    }
                }
                $coverage = $coverage>5000000?5000000:$coverage;
                $price = bcdiv(bcmul($coverage,$UserProject['schedule_day'],20),365,20);
                break;
            case 5:
                // 5   建筑行业安责险 确认ok
                $coverage = 1;
                $_coverage = 0;
                $UserProject['cost'] = $UserProject['cost']>2000000?$UserProject['cost']:2000000;
                foreach ($_configure as $vo) {
                    foreach ($configure as $v) {
                        if($v['is_count']==1){
                            if($vo['id']==$v['id']&&$vo['value']!='无'){
                                if($v['type']==4){
                                    // 相加
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $_coverage = bcadd($_coverage,$par[1],20);
                                        }
                                    }
                                }else if($v['type']==1){
                                    // 相乘
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $coverage = bcdiv(bcmul($coverage,$par[1],20),100,20);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $_coverage = bcdiv($_coverage,100,20);
                $coverage = bcmul($coverage,$_coverage,20);
                foreach ($configure as $v) {
                    if($v['status']==0&&$v['is_count']==1){
                        if($UserProject[$v['title']]>=0){
                            $parameter=explode(PHP_EOL,$v['parameter']);
                            foreach ($parameter as $value) {
                                $par=explode('|',$value);
                                $_par=explode('~',$par[0]);
                                if($UserProject[$v['title']]>=$_par[0]&&$UserProject[$v['title']]<$_par[1]){
                                    $coverage = bcdiv(bcmul($coverage,$par[1],20),100,20);
                                }
                            }
                        }
                    }
                }
                $price = bcmul($coverage,$UserProject['cost'],2);
                break;
            case 6:
                // 6   非建筑行业安责险 ok
                $coverage = 1;
                $cost = (int)$UserProject['cost'];
                foreach ($_configure as $vo) {
                    foreach ($configure as $v) {
                        if($v['is_count']==1){
                            if($vo['id']==$v['id']){
                                if($v['type']==4){
                                    // 相加
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $price = bcadd($price,$par[1],20);
                                        }
                                    }
                                }else if($v['type']==1){
                                    // 相乘
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $coverage = bcdiv(bcmul($coverage,$par[1],20),100,20);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $price = bcdiv(bcmul($coverage,$price,20),100,20);
                foreach ($configure as $v) {
                    if($v['status']==0&&$v['is_count']==1){
                        if($UserProject[$v['title']]>=0){
                            $parameter=explode(PHP_EOL,$v['parameter']);
                            foreach ($parameter as $value) {
                                $par=explode('|',$value);
                                $_par=explode('~',$par[0]);
                                if($cost>$_par[0]&&$cost<=$_par[1]){
                                    $price = bcdiv(bcmul($price,$par[1],20),100,20);
                                }
                            }
                        }
                    }
                }
                $num = (int)ceil(bcdiv($UserProject['cost'],50,20));
                $price = bcmul($price,$num,2);
                break;
            case 7:
                // 7   完工险 确认ok
                $coverage = 1;
                foreach ($configure as $v) {
                    // 自主计算
                    if($v['status']==0&&$v['is_count']==1){
                        if($UserProject[$v['title']]>=0){
                            $parameter=explode(PHP_EOL,$v['parameter']);
                            foreach ($parameter as $value) {
                                $par=explode('|',$value);
                                $_par=explode('~',$par[0]);
                                if($UserProject[$v['title']]>$_par[0]&&$UserProject[$v['title']]<=$_par[1]){
                                    $coverage = bcdiv(bcmul($coverage,$par[1],20),100,20);
                                }
                            }
                        }
                    }
                    // 用户选择计算
                    if($v['status']==1&&$v['is_count']==1){
                        if($v['type']==4){
                            foreach ($_configure as $vo) {
                                if($vo['id']==$v['id']){
                                    $parameter=explode(PHP_EOL,$v['parameter']);
                                    foreach ($parameter as $value) {
                                        $par=explode('|',$value);
                                        if ($par[0]==$vo['value']) {
                                            $coverage = bcdiv(bcmul($coverage,$par[1],20),100,20);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $price = bcdiv(bcmul($coverage,$UserProject['schedule_day'],20),365,20);
                $price = bcmul($price,$UserProject['cost'],20);
                break;
            case 8:
                // 8   建工意外险（分包）ok
                $price = 1;
                foreach ($configure as $v) {
                    if($v['is_count']==1){
                        foreach ($_configure as $vo) {
                            if($v['type']==2){
                                // 填空
                                $price = bcmul($price,$vo['value'],20);
                            }
                        }
                    }
                }
                break;
            default:
        }
        return $price;
    }

    /**
     * 提交报价
     **/
    public function reckon()
    {
        if (!$this->request->isPost()) {
            return json(['code'=>'0','message'=>"当前请求不合法！"]);
        }
        if (!$this->userInfo['tel']) {
            return json(['code'=>'98','message'=>"未授权手机号"]);
        }
        $post = $this->request->post();
        if($post['pid']!=2){
            $rule = [
                'pid|请选择项目'   => 'require',
                'cid|项目/公司' => 'require',
                'num|份数' => 'require'
            ];
            try {
                parent::validate($post, $rule);
            } catch (\Exception $e) {
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
        }

        $this->projectModel = new project();
        $this->configureModel = new ProjectConfigure();
        $this->UserProjectModel = new UserProject();

        $project = $this->projectModel->where('status', 1)->find($post['pid']);
        if (empty($project)) {
            return json(['code'=>'0','message'=>"数据不存在"]);
        }
        $UserProject = [];
        if(in_array($post['pid'],[1,3,4,5,6,7])){
            $UserProject = $this->UserProjectModel->field('applicant,assured,project,name,tel,province,city,address,start,end,schedule,business,cost,enterprise,schedule_day')->where('uid', $this->userInfo['id'])->where('pid', $project['id'])->where('id', $post['cid'])->find()->toarray();
            if (empty($UserProject)) {
                return json(['code'=>'0','message'=>"数据不存在"]);
            }
        }

        $configure = $this->configureModel->field('id,proportion,title,parameter,required,type,is_count,status')->where('p_id', $post['pid'])->select()->toarray();
        if (empty($configure)) {
            return json(['code'=>'0','message'=>"数据不存在"]);
        }
        $_ids = array_column($post['configure'],'id');
        $datas = '';
        foreach ($configure as $vo) {
            if($vo['required']==1&&$vo['status']==1){
                if(!in_array($vo['id'], $_ids)){
                    return json(['code'=>'0','message'=>$vo['title'].'不能为空']);
                }
            }
            if(in_array($vo['id'], $_ids)&&$vo['status']==1&&$vo['type']!=5){
                foreach ($post['configure'] as $v) {
                    if($v['id']==$vo['id']){
                        if($vo['required']==1&&$v['value']==''){
                            return json(['code'=>'0','message'=>$vo['title'].'不能为空']);
                        }
                        if($v['value']!=''){
                            $datas .= $vo['title'].':'.$v['value'].PHP_EOL;
                        }
                        if($vo['id']=='21' && $post['pid']==2){
                            $UserProject['tel'] = $v['value'];
                        }
                    }
                }
            }
        }
        $price = $this->getPrice($configure,$post['configure'],$post['pid'],$UserProject);
        $post['num'] = $post['num']?$post['num']:1;
        $rate = bcdiv($project['rate'],100,20);
        if($post['pid']==1){
            $_price = bcmul($UserProject['cost'],$rate,20);
            $price = bcadd($price,$_price,2);
        }else{
            $price = bcmul($price,$rate,2);
        }
        $price = bcmul($price,$post['num'],2);
        $UserProject['datas'] = $datas;
        $UserProject['price'] = $price;
        $UserProject['pid'] = $post['pid'];
        $UserProject['cid'] = isset($post['cid'])?$post['cid']:0;
        $UserProject['num'] = $post['num'];
        $UserProject['uid'] = $this->userInfo['id'];
        $UserProject['sid'] = $this->sid;
        $UserProject['cuid'] = $this->cuid;
        $this->UserReckonModel = new UserReckon();

        try {
            $save = $this->UserReckonModel->save($UserProject);
        } catch (\Exception $e) {
            return json(['code'=>'0','message'=>$e->getMessage()]);
        }
        if ($save) {
            if ($this->cuid) {
                $store = Db::name('storeuser')->field('nickname,phone')->where('id', $this->cuid)->find();
            }elseif ($this->sid) {
                $store = Db::name('store')->alias('s')->field('a.nickname,a.phone')->where('s.id', $this->sid)->join('system_admin a','a.id=s.admin_id')->find();
            }else{
                $store = [];
            }
            if(!$store){
                $store['nickname'] = sysconfig('site', 'site_pname');
                $store['phone'] = sysconfig('site', 'site_phone');
            }
            if(isset($UserProject['tel'])&&isset($UserProject['name'])){
                sendTemplateAliSMS($UserProject['tel'],['name'=>$UserProject['name'],'product'=>$project['title'],'num'=>$price.'元','shopOwnerName'=>$store['nickname'].' ','phone'=>$store['phone']],'SMS_259625728');
                sendTemplateAliSMS($store['phone'],['name'=>$UserProject['name'],'phone'=>$UserProject['tel'],'product'=>$project['title']],'SMS_259625454');
            }
            return json(['code'=>'1','message'=>"计算成功",'result'=>'您的保费预计'.$price.'元','datas'=>$datas]);
        }else{
            return json(['code'=>'0','message'=>"计算失败"]);
        }
    }
}