<?php
/**
 * Created by Aliz
 * User: 646594159@qq.com
 * Date: 2021/7/20
 * Time: 10:00
 */
namespace app\api\controller;
use app\BaseController;
use app\admin\model\ArticleLists;
use app\admin\model\Project;
use app\admin\model\Banner;
use app\admin\model\ProjectConfigure;
use app\admin\model\User;
use app\admin\model\UserProject;
use think\facade\Env;
use think\facade\Db;
use EasyAdmin\tool\CommonTool;

class Index extends BaseController
{
    protected $userId;
    protected function initialize()
    {
        $token = $this->request->param('token/s','');
        $sid = $this->request->param('sid/d',0);
        $this->UserModel = new User();
        $this->checkLogin($token);
        $data = [
            'sid'         => $sid,
            'uid'         => $this->userId,
            'url'         => $this->request->url(),
            'title'       => '',
            'ip'          => CommonTool::getRealIp(),
            'month'       => date('Y-m'),
            'create_time' => time(),
        ];
        Db::name('user_views')->insert($data);
    }

    public function index(){

    }

    /**
     * 判断用户是否登录
     **/
    protected function checkLogin($token = ''){
        if(empty($token)){
            $this->userId = 0;
        }
        $this->userId = Db::name('user')->where(['token'=>$token])->value('id');
        if(!$this->userId){
            $this->userId = 0;
        }
    }

    /**
     * 获取用户token
    **/
    public function code2Session(){
        if ($this->request->isPost()) {
            $code=$this->request->param('code');
            $sid = $this->request->param('sid/d',0);
            $cuid = $this->request->param('cuid/d',0);
            if(!empty($code)) {
                $appid = Env::get('WECHAT.APPID', '');
                $appsecrt = Env::get('WECHAT.APPSECRT', '');
                $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$appid.'&secret='.$appsecrt.'&js_code='.$code.'&grant_type=authorization_code'; //客户号
                $result = https_request($url);
                $result = json_decode($result,true);
                if(empty($result['openid'])){
                    return json(['code'=>'0','message'=>'微信授权失败，请重新授权！']);
                }else{
                    $token = md5($result['openid'].$result['session_key'].time());
                    $tel = '';
                    $time = time();
                    $this->model = new User();
                    $info = $this->model->where('openid',$result['openid'])->find();
                    if (empty($info)) {
                        $data = array();
                        $data['nickname'] = $result['openid'];
                        $data['name'] = $result['openid'];
                        $data['tel'] = '';
                        $data['status'] = 1;
                        $data['openid'] = $result['openid'];
                        $data['unionid'] = '';
                        $data['last_login_time'] = $time;
                        $data['last_login_ip'] = '';
                        $data['login_count'] = 1;
                        $data['create_time'] = $time;
                        $data['update_time'] = $time;
                        $data['token'] = $token;
                        $data['session_key'] = $result['session_key'];
                        $data['avatar'] = '';
                        // $data['sid'] = $sid;
                        // $data['cuid'] = $cuid;
                        $save = $this->model->save($data);
                    }else{
                        $data = array();
                        $data['last_login_time'] = $time;
                        $data['token'] = $token;
                        $data['login_count'] = $info['login_count']+1;
                        $data['session_key'] = $result['session_key'];
                        $tel = $info['tel'];
                        $save = $this->model->where('id',$info['id'])->save($data);
                    }
                    if ($save) {
                        return json(['code'=>'1','message'=>"授权成功",'result'=>['token'=>$token,'tel'=>$tel,'sid'=>$sid,'cuid'=>$cuid]]);
                    }else{
                        return json(['code'=>'0','message'=>"微信授权失败，请重新授权！"]);
                    }
                }
            }
            return json(['code'=>'0','message'=>'微信授权失败，请重新授权！']);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 广告
    **/
    public function banner(){
        if ($this->request->isPost()) {
            $this->model = new Banner();
            try{
                $list = $this->model->field('id,image')->where('status', 1)->order('sort', 'desc')->limit(5)->select();
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 文章列表
    **/
    public function articles(){
        if ($this->request->isPost()) {
            $this->model = new ArticleLists();
            try{
                $list = $this->model->alias('l')->field('l.id,l.title,l.images,l.describe,c.title as cate_name')->leftJoin('article_cate c','l.cate_id = c.id')->where('l.status', 1)->order('l.sort', 'desc')->paginate(10)->toArray();
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 文章详情
    **/
    public function articlesInfo(){
        if ($this->request->isPost()) {
            $this->model = new ArticleLists();
            $id = $this->request->param('id/d','');
            try{
                $info = $this->model->alias('l')->field('l.title,l.images,l.describe,l.details,c.title as cate_name')->leftJoin('article_cate c','l.cate_id = c.id')->where('l.status', 1)->where('l.id', $id)->find();
                if (empty($info)) {
                    return json(['code'=>'0','message'=>'文章不存在']);
                }
                $info['details'] = html_entity_decode($info['details']);
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 项目列表
    **/
    public function project(){
        if ($this->request->isPost()) {
            $this->model = new project();
            try{
                $list = $this->model->field('id,title,logo,type')->where('status', 1)->order('sort', 'desc')->paginate(10)->toArray();
            }catch (\Exception $e){
                return json(['code'=>'0','message'=>$e->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }


}
