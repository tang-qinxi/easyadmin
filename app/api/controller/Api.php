<?php
/**
 * Created by Aliz
 * User: 646594159@qq.com
 * Date: 2021/7/20
 * Time: 10:00
 */
namespace app\api\controller;
use app\BaseController;
use think\facade\Env;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Paginator;
// use EasyWeChat\Factory;
use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use GatewayWorker\Lib\Gateway;
use Workerman\Worker;
use think\worker\Application;

class Api extends BaseController
{
    protected $userInfo;
    protected function initialize()
    {
        $token = $this->request->param('token/s','');
        $this->checkLogin($token);
    }

    /**
     * 判断用户是否登录
     **/
    protected function checkLogin($token = ''){
        if(empty($token)){
            echo  json_encode(["code"=>"99","message"=>"登录失效，请重新登录"]);exit();
        }
        $this->userInfo = Db::name('user')->where(['token'=>$token])->find();
        if(!$this->userInfo){
            echo  json_encode(["code"=>"99","message"=>"登录失效，请重新登录"]);exit();
        }
    }

    /**
     * 微信SDK签名
    **/
    public function signature(){
        // $appid = Env::get('WECHAT.APPID', false);
        // $appsecrt = Env::get('WECHAT.APPSECRT', false);
        // $config = [
        //     'app_id' => $appid,
        //     'secret' => $appsecrt,
        //     'response_type' => 'array', // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
        // ];
        // $Js = Factory::officialAccount($config);
        // $a = $Js->jssdk->buildConfig(array("getLocation","openLocation","scanQRCode","chooseImage","previewImage","uploadImage"), false, false, false,[],@$_SERVER['HTTP_REFERER']);
        $a = ["debug"=>false,"beta"=>false,"jsApiList"=>["getLocation","openLocation","scanQRCode","chooseImage","previewImage","uploadImage"],"openTagList"=>[],"appId"=>"wx837ab0f8e740150d","nonceStr"=>"VzkhiKD53J","timestamp"=>1635926573,"url"=>"https://pm.e-stronger.com/user/my.html","signature"=>"5e1628a14f957df97ef928281c7b49339911e6a1"];
        return json(['code'=>'1','message'=>"成功",'result'=>$a]);
    }

    /**
     * 我的头像昵称
    **/
    public function userInfo(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $info = ['avatar'=>$userInfo['avatar'],'nickname'=>$userInfo['nickname'],'name'=>$userInfo['name'],'tel'=>$userInfo['tel'],'status'=>$userInfo['status']];
                $info['auctions'] = Db::name('activity')->where('user_id',$userInfo['id'])->where('status',0)->count();
                $info['auctioning'] = Db::name('activity')->where('user_id',$userInfo['id'])->where('status','in',[1,4])->count();
                $info['audits'] = Db::name('activity_user')->alias('u')->join('activity a','u.activity_id = a.id','LEFT')->where('u.user_id',$userInfo['id'])->where('u.status',0)->count();
                $info['audited'] = Db::name('activity_user')->alias('u')->join('activity a','u.activity_id = a.id','LEFT')->where('u.user_id',$userInfo['id'])->where('u.status',1)->where('a.status','in',[0,1,4])->count();
                $info['auditing'] = Db::name('activity_user')->alias('u')->join('activity a','u.activity_id = a.id','LEFT')->where('u.create_user_id',$userInfo['id'])->where('u.status',0)->count();
                $info['orders'] = Db::name('order')->where('user_id',$userInfo['id'])->where('status',0)->count()+Db::name('order')->where('c_user_id='.$userInfo['id'])->where('c_status',0)->count();
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 图片上传
    **/
    public function uploadImage(){
        if($this->request->post()){
            try{
                $img = $this->request->param('file/s','');
                if(empty($img)){
                    return json(['code'=>'0','message'=>'请选择图片']);
                }
                // $param = $this->request->param();
                // $img = $param['file'];
                //文件夹日期
                $ymd = date("Ymd");
                //图片路径地址
                $basedir = 'upload/image/'.$ymd.'';
                $fullpath = $basedir;
                if(!is_dir($fullpath)){
                    mkdir($fullpath,0777,true);
                }
                $types = empty($types)? array('jpg', 'gif', 'png', 'jpeg'):$types;
                $img = str_replace(array('_','-'), array('/','+'), $img);
                $b64img = substr($img, 0,100);
                if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $b64img, $matches)){
                    $type = $matches[2];
                    if(!in_array($type, $types)){
                        return json(['code'=>'0','message'=>"图片格式不正确，只支持 jpg、png、jpeg哦！"]);
                    }
                    $img = str_replace($matches[1], '', $img);
                    $img = base64_decode($img);
                    $photo = '/'.md5(date('YmdHis').rand(1000, 9999)).'.'.$type;
                    file_put_contents($fullpath.$photo, $img);
                    return json(['code'=>'1','message'=>"上传成功",'result'=>['image_url'=>'/'.$basedir.$photo]]);
                }
                return json(['code'=>'0','message'=>"请选择要上传的图片"]);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,清用POST方式请求"]);
        }
    }

    /**
     * 提交建议
    **/
    public function addProposal(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $data = [];
                $data['type'] = $this->request->param('type/s','');
                $data['contents'] = $this->request->param('contents/s','');
                $data['tel'] = $this->request->param('tel/s','');
                $data['images'] = $this->request->param('images','');
                if(is_array($data['images'])){
                    $data['images'] =  implode("|",$this->request->param('images',''));
                }
                $data['user_id'] = $userInfo['id'];
                $data['nickname'] = $userInfo['nickname'];
                $data['avatar'] = $userInfo['avatar'];
                $data['create_time'] = time();
                $collection = Db::name('proposal_log')->insert($data);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>'']);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 创建拍卖
    **/
    public function addActivity(){
        if ($this->request->isPost()) {
            try{
                $data = $this->request->param(['cid','corporate_name','name','tel','type','start_time','end_time','second','is_open','invitation_code','goods_id','product_name','provenance','specifications','weight','unit','reserve_price','is_tax','pay_day','range','ratio','delivery_date','last_delivery_date','transfer','address','location','describe','s_o_s','norm','images','yzm_code','activity_sn','safe_price','price_open','distribution']);
                if(empty($data['corporate_name'])){
                    return json(['code'=>'0','message'=>'请输入公司名']);
                }
                if(empty($data['name'])){
                    return json(['code'=>'0','message'=>'请输入联系人']);
                }
                if(empty($data['tel'])){
                    return json(['code'=>'0','message'=>'请输入真实手机号']);
                }
                if(!isset($data['type'])||!in_array($data['type'],[1,2,3,4,5,6,7,8])){
                    return json(['code'=>'0','message'=>"请选择拍卖方式"]);
                }
                if(empty($data['start_time'])){
                    return json(['code'=>'0','message'=>'请选择开拍时间']);
                }
                if(($data['type']==1||$data['type']==2||$data['type']==6)&&empty($data['second'])){
                    return json(['code'=>'0','message'=>'请选择间隔时间']);
                }
                if(($data['type']==3||$data['type']==4||$data['type']==5||$data['type']==7||$data['type']==8)&&empty($data['end_time'])){
                    return json(['code'=>'0','message'=>'请选择结束时间']);
                }
                if(empty($data['invitation_code'])&&$data['is_open']==0){
                    return json(['code'=>'0','message'=>'请输入邀请码']);
                }

                if(empty($data['goods_id'])){
                    return json(['code'=>'0','message'=>'请选择产品']);
                }
                if(empty($data['product_name'])){
                    return json(['code'=>'0','message'=>'请选择产品']);
                }
                if(empty($data['provenance'])){
                    return json(['code'=>'0','message'=>'请输入品类']);
                }
                if(empty($data['specifications'])){
                    return json(['code'=>'0','message'=>'请选择规格']);
                }
                if(empty($data['weight'])){
                    return json(['code'=>'0','message'=>'请输入重量']);
                }
                if(empty($data['unit'])){
                    return json(['code'=>'0','message'=>'请选择单位']);
                }

                if(empty($data['reserve_price'])){
                    return json(['code'=>'0','message'=>'请输入起拍价格']);
                }
                if($data['type']==2&&empty($data['safe_price'])){
                    return json(['code'=>'0','message'=>'请输入保底价格']);
                }
                if($data['type']==2&&$data['safe_price']<=0){
                    return json(['code'=>'0','message'=>'请输入保底价格']);
                }
                if($data['type']==2&&$data['safe_price']>=$data['reserve_price']){
                    return json(['code'=>'0','message'=>'保底价格不能高于起拍价格']);
                }
                if($data['type']==6&&empty($data['safe_price'])){
                    return json(['code'=>'0','message'=>'请输入最低价格']);
                }
                if($data['type']==6&&$data['safe_price']<=0){
                    return json(['code'=>'0','message'=>'请输入最低价格']);
                }
                if($data['type']==6&&$data['safe_price']>=$data['reserve_price']){
                    return json(['code'=>'0','message'=>'最低价格不能高于起拍价格']);
                }
                if(empty($data['pay_day'])){
                    return json(['code'=>'0','message'=>'请输入付款周期']);
                }
                if(empty($data['range'])&&($data['type']==1||$data['type']==2||$data['type']==6)){
                    return json(['code'=>'0','message'=>'请输入最小加/减价幅度单位']);
                }
                $ratio = (int)$data['ratio'];
                if($data['ratio']!=$ratio){
                    return json(['code'=>'0','message'=>'合同保证金比例范围是0%~100%，请重新输入']);
                }
                if($data['ratio']>100||$data['ratio']<0){
                    return json(['code'=>'0','message'=>'合同保证金比例范围是0%~100%，请重新输入']);
                }
                if(empty($data['delivery_date'])){
                    return json(['code'=>'0','message'=>'请选择提货开始日期']);
                }
                if(empty($data['last_delivery_date'])){
                    return json(['code'=>'0','message'=>'请选择提货结束日期']);
                }
                if(empty($data['s_o_s'])){
                    return json(['code'=>'0','message'=>'请上传油样图片']);
                }
                if(empty($data['norm'])){
                    return json(['code'=>'0','message'=>'请上传指标图片']);
                }
                $data['images'] = @$data['images']?$data['images']:'';
                // if(empty($data['images'])){
                //     return json(['code'=>'0','message'=>'请上传商品描述图片']);
                // }
                $is_tax = $data['is_tax']?'含税':'不含税';
                $data['title'] = $data['provenance'].'_'.$data['specifications'].$data['product_name'].'('.$is_tax.')';
                $data['is_open'] = $data['invitation_code']!=''?0:1;
                $data['start_time'] = strtotime($data['start_time']);
                $data['end_time'] = $data['end_time']?strtotime($data['end_time']):null;
                $data['end_time'] = $data['type']==2?null:$data['end_time'];
                $data['images'] = json_encode($data['images']);
                $userInfo = $this->userInfo;
                $data['user_id'] = $userInfo['id'];
                $data['create_time'] = time();
                $data['update_time'] = time();
                if($data['type']==5){
                    $data['second'] = $data['end_time']-$data['start_time'];
                }
                $data['final_price'] = $data['reserve_price'];
                $nums = Db::name('activity')->where('user_id',$userInfo['id'])->where('tel',$data['tel'])->count();
                if($nums<1){
                    if(empty($data['yzm_code'])){
                        return json(['code'=>'0','message'=>'请输入验证码']);
                    }
                    $this->checkSms($data['tel'],$data['yzm_code'],$userInfo['id']);
                }

                $k_flag = Db::name('mall_user_cache')->where('keyword', $data['provenance'])->find();
                if(!$k_flag){
                    Db::name('mall_user_cache')->insert(['keyword'=>$data['provenance'],'user_id'=>$this->userInfo['id'],'create_time'=>time()]);
                }
                unset($data['yzm_code']);
                if(@$data['activity_sn']){
                    $old_info = Db::name('activity')->where('activity_sn',$data['activity_sn'])->find();
                    if($old_info['status']>0){
                        return json(['code'=>'0','message'=>"保存失败，拍卖已经开始"]);
                    }
                    $flag_count = Db::name('activity_user')->where('activity_id',$old_info['id'])->count('id');
                    if($flag_count>0){
                        return json(['code'=>'0','message'=>"保存失败，已有买家申请了该拍卖场了"]);
                    }
                    cache($data['activity_sn'].'_reservePrice',$data['reserve_price']);
                    Db::name('activity')->where('activity_sn',$data['activity_sn'])->update($data);
                    $msg = '编辑';
                }else{
                    $data['activity_sn'] = 'PM'.date('ymdHis').rand(1000, 9999);
                    Db::name('activity')->insert($data);
                    $msg = '创建';
                    cache($data['activity_sn'].'_reservePrice',$data['reserve_price']);
                }
                if(!empty($data['start_time'])){
                    $data['start_time'] = date('Y-m-d H:i:s',$data['start_time']);
                }
                if(!empty($data['end_time'])){
                    $data['end_time'] = date('Y-m-d H:i:s',$data['end_time']);
                }
                return json(['code'=>'1','message'=>$msg."成功",'result'=>$data]);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 删除拍卖
    **/
    public function delActivity(){
        if ($this->request->isPost()) {
            try{
                $activity_sn = $this->request->param('activity_sn/s','');
                if($activity_sn==''){
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                }
                $userInfo = $this->userInfo;
                $map = ['user_id'=>$userInfo['id'],'activity_sn'=>$activity_sn];
                $activity_info = Db::name('activity')->where($map)->find();
                if($activity_info&&$activity_info['status']==0){
                    $activityInfo = Db::name('activity')->where('id',$activity_info['id'])->delete();
                    return json(['code'=>'1','message'=>"删除成功",'result'=>'']);
                }else if($activity_info&&$activity_info['status']>=1){
                    return json(['code'=>'0','message'=>"活动已开始了，不能删除了"]);
                }else{
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                }
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我创建的拍卖
    **/
    public function myActivity(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $map[] = ['user_id','=', $userInfo['id']];
                $status = $this->request->param('status/d','-1');
                $keywords = $this->request->param('keywords/s','');
                if($status=='0'){
                    $map[] = ['status','=', 0];
                }
                if($status=='2'){
                    $map[] = ['status','in', [2,3]];
                }
                if($status=='1'){
                    $map[] = ['status','in', [1,4]];
                }
                if($keywords!=''){
                    $map[] = ['title','like','%'.$keywords.'%'];
                }
                $list = Db::name('activity')->field('id,activity_sn,title,cid,s_o_s,status,view,start_time,end_time,type')->where($map)->order('id', 'desc')->paginate(10)->toArray();
                foreach ($list['data'] as $k => $v){
                    $list['data'][$k]['view'] = Db::name('activity_user')->where('activity_id',$v['id'])->count();
                }
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的订单
    **/
    public function myOrders(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $map = [];
                $whereRaw = 'o.user_id='.$userInfo['id'].' OR o.c_user_id='.$userInfo['id'];
                $status = $this->request->param('status/d','-1');
                $keywords = $this->request->param('keywords/s','');
                if($status=='0'){
                    $whereRaw = '(o.user_id='.$userInfo['id'].' AND o.status=0) OR (o.c_user_id='.$userInfo['id'].' AND o.c_status=0)';
                }
                if($status=='1'){
                    $whereRaw = '(o.user_id='.$userInfo['id'].' AND o.status=1) OR (o.c_user_id='.$userInfo['id'].' AND o.c_status=1)';
                }
                if($keywords!=''){
                    $map[] = ['a.title','like','%'.$keywords.'%'];
                }
                $list = Db::name('order')->alias('o')->field('o.c_status,o.c_user_id,o.status,o.order_sn,order_address,order_location,o.weight,o.unit,o.final_price,a.title,a.corporate_name,a.name,a.s_o_s,a.tel,a.address,a.location,a.distribution,a.is_tax,a.delivery_date,a.last_delivery_date,a.ratio,au.name as order_name,au.tel as order_tel')->leftJoin('activity a','o.activity_sn = a.activity_sn')->leftJoin('activity_user au','a.id = au.activity_id and o.user_id = au.user_id')->where($map)->whereRaw($whereRaw)->order('o.id', 'desc')->paginate(10)->toArray();
                foreach ($list['data'] as $k => $v){
                    $list['data'][$k]['is_creator'] = $v['c_user_id']==$userInfo['id']?true:false;
                    $list['data'][$k]['status'] = $v['c_user_id']==$userInfo['id']?$v['c_status']:$v['status'];
                }
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的订单详情
    **/
    public function getOrderInfo(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $order_sn = $this->request->param('order_sn/s','');

                $info = Db::name('order')->alias('o')->field('o.c_status,o.c_user_id,o.status,o.order_sn,order_address,order_location,o.weight,o.unit,o.final_price,a.title,a.corporate_name,a.name,a.s_o_s,a.tel,a.address,a.location,a.distribution,a.is_tax,a.delivery_date,a.last_delivery_date,a.ratio,au.name as order_name,au.tel as order_tel')->leftJoin('activity a','o.activity_sn = a.activity_sn')->leftJoin('activity_user au','a.id = au.activity_id and o.user_id = au.user_id')->whereRaw('o.user_id='.$userInfo['id'].' OR o.c_user_id='.$userInfo['id'])->where('o.order_sn',$order_sn)->order('o.id', 'desc')->find();
                $info['is_creator'] = $info['c_user_id']==$userInfo['id']?true:false;
                $info['status'] = $info['c_user_id']==$userInfo['id']?$info['c_status']:$info['status'];

                if(empty($info)){
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                }
                return json(['code'=>'1','message'=>"成功",'result'=>$info]);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 评价
    **/
    public function appraise(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $order_sn = $this->request->param('order_sn/s','');
                $goods_score = $this->request->param('goods_score/d',0);
                $service_score = $this->request->param('service_score/d',0);
                if(!$goods_score||!$goods_score){
                    return json(['code'=>'0','message'=>"请评分"]);
                }
                $remark = $this->request->param('remark/s','');
                if(empty($remark)){
                    return json(['code'=>'0','message'=>"请输入具体评价"]);
                }
                $images = $this->request->param('images',[]);
                if(empty($images)){
                    return json(['code'=>'0','message'=>"请上传图片"]);
                }
                $map = ['order_sn'=>$order_sn];
                $info = Db::name('order')->where($map)->find();
                if($info['user_id']!=$userInfo['id']&&$info['c_user_id']!=$userInfo['id']){
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                }
                if($info['user_id']==$userInfo['id']){
                    // 买家
                    if($info['status']){
                        return json(['code'=>'0','message'=>"订单已评价，请勿重复提交。"]);
                    }

                    $data = array(
                        'goods_score'=>$goods_score,
                        'service_score'=>$service_score,
                        'remark'=>$remark,
                        'images'=>json_encode($images),
                        'update_time'=>time(),
                        'status'=>1,
                    );
                    $flag = Db::name('order')->where($map)->update($data);
                    if($flag){
                        return json(['code'=>'1','message'=>"保存成功",'result'=>[]]);
                    }
                }else{
                    // 卖家
                    if($info['c_status']){
                        return json(['code'=>'0','message'=>"订单已评价，请勿重复提交。"]);
                    }

                    $data = array(
                        'c_goods_score'=>$goods_score,
                        'c_service_score'=>$service_score,
                        'c_remark'=>$remark,
                        'c_images'=>json_encode($images),
                        'update_time'=>time(),
                        'c_status'=>1,
                    );
                    $flag = Db::name('order')->where($map)->update($data);
                    if($flag){
                        return json(['code'=>'1','message'=>"保存成功",'result'=>[]]);
                    }

                }
                return json(['code'=>'0','message'=>"保存失败"]);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的初始信息
    **/
    public function myActivityInfo(){
        if ($this->request->isPost()) {
            try{
                $type = $this->request->param('type/s','');
                $field = '*';
                if(!empty($type)){
                    $info = Db::name('activity')->where(['user_id'=>$this->userInfo['id'],'type'=>$type])->order('id', 'desc')->find();
                }else{
                    $info = Db::name('activity')->where(['user_id'=>$this->userInfo['id']])->order('id', 'desc')->find();
                }
                if(!empty($info['images'])){
                    $info['images'] = json_decode($info['images'],true);
                }
                $info['start_time'] = date('Y-m-d H:i',strtotime('+30 minutes'));
                $info['end_time'] = date('Y-m-d H:i',strtotime('+90 minutes'));
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 我的初始信息
    **/
    public function getGoods(){
        if ($this->request->isPost()) {
            try{
                $cid = $this->request->param('cid/d',1);
                $goods_id = $this->request->param('goods_id/d',1);
                $goods = Db::name('mall_goods')->field('id as goods_id,title,ico,act_ico,class')->where(['cate_id'=>$cid,'status'=>1])->order('sort', 'desc')->select()->toArray();
                foreach ($goods as &$v){
                    $v['class'] = explode("|",$v['class']);
                }
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$goods]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 拍卖详情
    **/
    public function activityInfo(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            if($activity_sn==''){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            try{
                $field = '*';
                $info = Db::name('activity')->field($field)->where(['activity_sn'=>$activity_sn])->find();
                if($info){
                    if($info['status']==0){
                        $info['second'] = $info['start_time']-time();
                    }
                    if($info['status']==1&&$info['type']>2){
                        $info['second'] = $info['end_time']-time();
                    }
                    if($info['status']==1&&$info['type']==1){
                        $info['second'] = cache($activity_sn.'_countdown');
                    }
                    if( $info['status']==1&&$info['type']==2){
                        $info['second'] = $info['second']-(time()-cache($activity_sn.'_lastTime'));
                    }
                    if($info['status']>1){
                        $info['second'] = -1;
                    }
                    $info['start_time'] = date('Y-m-d H:i:s',$info['start_time']);
                    $info['end_time'] = date('Y-m-d H:i:s',$info['end_time']);
                    $info['images'] = json_decode($info['images'],true);
                    if (is_array($info['images'])) {
                        array_unshift($info['images'],$info['s_o_s'],$info['norm']);
                    }else{
                        $info['images'] = [$info['s_o_s'],$info['norm']];
                    }
                    $info['winner'] = json_decode($info['winner'],true);
                    $info['user_corporate_name'] = $this->userInfo['corporate_name'];
                    $info['user_name'] = $this->userInfo['name'];
                    $info['user_tel'] = $this->userInfo['tel'];
                    $info['order_sn'] = '';
                    if($info['user_id']==$this->userInfo['id']){
                        // 卖方
                        $info['is_self'] = true; //卖家
                        $info['is_check'] = true; //
                        $info['join_flag'] = false;
                        $info['is_winner'] = false;
                        if(is_array($info['winner'])){
                            foreach ($info['winner'] as &$vo ){
                                $vo['from_client_name'] = $vo['name'];
                                unset($vo['id']);
                                unset($vo['name']);
                            }
                        }

                    }else{
                        // 买方
                        $info['is_self'] = false; //买家
                        $info['is_check'] = true;
                        if($info['is_open']==0){
                            $info['is_check'] = Db::name('activity_user_check')->where('activity_id',$info['id'])->where('user_id',$this->userInfo['id'])->value('flag');
                            $info['is_check'] = $info['is_check']==1?true:false;
                        }
                        $info['join_flag'] = Db::name('activity_user')->where('activity_id',$info['id'])->where('user_id',$this->userInfo['id'])->value('flag');
                        $info['join_flag'] = is_null($info['join_flag'])?-1:$info['join_flag'];
                        $info['is_winner'] = false;
                        $is_winner = false;
                        $info['self_weight'] = 0;
                        if(is_array($info['winner'])){
                            foreach ($info['winner'] as &$vo ){
                                if($vo['id']==$this->userInfo['id']){
                                    $info['is_winner'] = true;
                                    $is_winner = true;
                                    $info['self_weight'] = @$vo['weight']?$vo['weight']:0;
                                    $info['order_sn'] = $vo['order_sn'];
                                }
                                unset($vo['id']);
                                unset($vo['tel']);
                                unset($vo['name']);
                            }
                        }
                        if(!$is_winner){
                            $info['tel'] = '******';
                        }
                    }
                }
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            $info['activity_log'] = [];
            if(isset($info['type'])&&in_array($info['type'],[1,3,4,7])&&in_array($info['status'],[2,3])){
                $activity_logs = Db::name('activity_log')->field('from_client_id,from_client_name,from_client_image,max(price) as price,time')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->group('from_client_id')->order('price desc')->select();
                foreach ($activity_logs as $k=>$v){
                    $info['activity_log'][$k]['from_client_name'] = $v['from_client_name'];
                    if($info['user_id']==$this->userInfo['id']||$v['from_client_id']==$this->userInfo['id']){
                        $info['activity_log'][$k]['from_client_name'] = Db::name('activity_user')->where(['activity_id'=>$info['id'],'user_id'=>$v['from_client_id']])->value('name');
                    }
                    $info['activity_log'][$k]['from_client_image'] = $v['from_client_image'];
                    $info['activity_log'][$k]['price'] = $v['price'];
                    $info['activity_log'][$k]['time'] = $v['time'];
                }
            }
            if(isset($info['type'])&&($info['type']==2||$info['type']==6)&&in_array($info['status'],[2,3])){
                $activity_logs = Db::name('activity_log')->field('from_client_id,from_client_name,from_client_image,min(price) as price,time')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->group('from_client_id')->order('price desc')->select();
                foreach ($activity_logs as $k=>$v){
                    $info['activity_log'][$k]['from_client_name'] = $v['from_client_name'];
                    if($info['user_id']==$this->userInfo['id']||$v['from_client_id']==$this->userInfo['id']){
                        $info['activity_log'][$k]['from_client_name'] = Db::name('activity_user')->where(['activity_id'=>$info['id'],'user_id'=>$v['from_client_id']])->value('name');
                    }
                    $info['activity_log'][$k]['from_client_image'] = $v['from_client_image'];
                    $info['activity_log'][$k]['price'] = $v['price'];
                    $info['activity_log'][$k]['time'] = $v['time'];
                }
            }
            if(isset($info['type'])&&in_array($info['type'],[3,4,7])&&$info['status']==1&&!$info['is_self']){
                $activity_logs = Db::name('activity_log')->field('from_client_id,from_client_name,from_client_image,max(price) as price,time')->where(['activity_sn'=>$activity_sn,'from_client_id'=>$this->userInfo['id']])->where(['type'=>'offer'])->group('from_client_id')->order('price desc')->select();
                foreach ($activity_logs as $k=>$v){
                    $info['activity_log'][$k]['from_client_name'] = $v['from_client_name'];
                    if($v['from_client_id']==$this->userInfo['id']){
                        $info['activity_log'][$k]['from_client_name'] = Db::name('activity_user')->where(['activity_id'=>$info['id'],'user_id'=>$v['from_client_id']])->value('name');
                    }
                    $info['activity_log'][$k]['from_client_image'] = $v['from_client_image'];
                    $info['activity_log'][$k]['price'] = $v['price'];
                    $info['activity_log'][$k]['time'] = $v['time'];
                }
            }
            if(isset($info['type'])&&in_array($info['type'],[3,4,7])&&$info['status']==1&&$info['is_self']){
                $activity_logs = Db::name('activity_log')->field('from_client_id,from_client_name,from_client_image,max(price) as price,time')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->group('from_client_id')->order('price desc')->select();
                foreach ($activity_logs as $k=>$v){
                    $info['activity_log'][$k]['from_client_name'] = $v['from_client_name'];
                    if($v['from_client_id']==$this->userInfo['id']){
                        $info['activity_log'][$k]['from_client_name'] = Db::name('activity_user')->where(['activity_id'=>$info['id'],'user_id'=>$v['from_client_id']])->value('name');
                    }
                    $info['activity_log'][$k]['from_client_image'] = $v['from_client_image'];
                    $info['activity_log'][$k]['price'] = $v['price'];
                    $info['activity_log'][$k]['time'] = $v['time'];
                }
            }
            if(isset($info['type'])&&in_array($info['type'],[5,8])&&in_array($info['status'],[2,3])){
                $activity_logs = Db::name('activity_log')->field('from_client_id,from_client_name,from_client_image,price as weight,num,time')->where(['activity_sn'=>$activity_sn,'num'=>$info['num']])->where(['type'=>'offer'])->select();
                foreach ($activity_logs as $k=>$v){
                    $info['activity_log'][$k]['from_client_name'] = $v['from_client_name'];
                    if($info['user_id']==$this->userInfo['id']||$v['from_client_id']==$this->userInfo['id']){
                        $info['activity_log'][$k]['from_client_name'] = Db::name('activity_user')->where(['activity_id'=>$info['id'],'user_id'=>$v['from_client_id']])->value('name');
                    }
                    $info['activity_log'][$k]['from_client_image'] = $v['from_client_image'];
                    $info['activity_log'][$k]['price'] = $v['weight'];
                    $info['activity_log'][$k]['time'] = $v['time'];
                }
            }


            $info['join_users'] = Db::name('activity_user')->where('activity_id',$info['id'])->where('user_id','<>',0)->where('flag',1)->limit(5)->column('from_client_image');
            $info['join_users_count'] = Db::name('activity_user')->where('activity_id',$info['id'])->where('user_id','<>',0)->where('flag',1)->count('id');
            $info['kftel'] = sysconfig('site', 'site_tel');
            $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.access_token().'&openid='.$this->userInfo['openid'].'&lang=zh_CN';
            $wx_user = http_curl($url,'');
            if(!empty($wx_user)&&@$wx_user['errcode']){
                $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.access_token(true).'&openid='.$this->userInfo['openid'].'&lang=zh_CN';
                $wx_user = http_curl($url,'');
            }
            $info['subscribe_flag'] = true;
            if(!empty($wx_user)&&@$wx_user['subscribe']==1){
                $info['subscribe_flag'] = false;
            }
            if (@$wx_user['errcode']==40001) {
                Cache::set('access_token','',1);
            }
            Db::name('activity')->where(['activity_sn'=>$activity_sn])->inc('view')->update();
            $info['user_id'] = $this->userInfo['id'];
            $info['view']++;
                $new_message = array(
                    'type'=>'viewdata', 
                    'view'=>$info['view'],
                    'join_users_count'=>$info['join_users_count'],
                );
                Gateway::sendToGroup($activity_sn ,json_encode($new_message));
            return json(['code'=>'1','message'=>"成功",'result'=>$info]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 拍卖详情(简信)
    **/
    public function getActivityInfo(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            if($activity_sn==''){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            try{
                $field = 'view,status,type,user_id,id';
                $info = Db::name('activity')->field($field)->where(['activity_sn'=>$activity_sn])->find();
                if($info){
                    if($info['user_id']==$this->userInfo['id']){
                        // 卖方
                        $info['is_self'] = true; //卖家
                        $info['is_check'] = true; //
                        $info['join_flag'] = false;
                    }else{
                        // 买方
                        $info['is_self'] = false; //买家
                        $info['is_check'] = true;
                        $info['join_flag'] = Db::name('activity_user')->where('activity_id',$info['id'])->where('user_id',$this->userInfo['id'])->value('flag');
                        $info['join_flag'] = is_null($info['join_flag'])?-1:$info['join_flag'];
                    }
                    $info['join_users_count'] = Db::name('activity_user')->where('activity_id',$info['id'])->where('flag',1)->count('id');
                    unset($info['user_id']);
                    unset($info['id']);
                    return json(['code'=>'1','message'=>"成功",'result'=>$info]);
                }
                return json(['code'=>'0','message'=>"参数异常"]);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 获取关键字
    **/
    public function getKeywords(){
        if ($this->request->isPost()) {
            $keywords = $this->request->param('keywords/s','');
           $map = [];
            if($keywords){
                $map[] = ['keyword','like','%'.$keywords.'%'];
            }
            $data = Db::name('mall_user_cache')->field('keyword')->where($map)->select()->toArray();
            if(empty($data)){
                $data = [["keyword"=>"中石油"],["keyword"=>"中石化"],["keyword"=>"中海油"]];
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$data]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 终止拍卖
    **/
    public function close(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $userInfo = $this->userInfo;
            $map = [];
            $map['type'] = 'offer';
            $map['activity_sn'] = $activity_sn;
            $info = Db::name('activity')->where(['activity_sn'=>$activity_sn,'user_id'=>$userInfo['id']])->find();
            if(!$info){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            if($info['status']==1||$info['status']==4){
                return json(['code'=>'0','message'=>"本场拍卖已经开始了，不能执行流拍操作"]);
            }
            if($info['status']==2||$info['status']==3){
                return json(['code'=>'0','message'=>"本场拍卖已经结束，不能执行流拍操作"]);
            }

            switch ($info['type']) {
                case 1:
                    $info['num'] = 0;
                    $new_price = cache($activity_sn.'_reservePrice');
                    break;
                case 2:
                    $info['num'] = 0;
                    $new_price = cache($activity_sn.'_reservePrice');
                    break;
                case 3:
                    $info['num'] = 0;
                    $new_price = $info['reserve_price'];
                    break;
                case 4:
                    $info['num'] = 0;
                    $new_price = $info['reserve_price'];
                    break;
                case 5:
                    $new_price = $info['final_price'];
                    break;
                default:
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                    break;
            }
            Db::name('activity')->where('id',$info['id'])->update(['status'=>3,'update_time'=>time(),'final_price'=>$new_price]);
            $new_data = array(
                'activity_sn'=>$activity_sn, 
                'type'=>'close', 
                'from_client_id'=>$userInfo['id'],
                'from_client_name' =>'拍卖场主',
                'from_client_image' =>'https://pm.e-stronger.com/user/images/avatar/user.png',
                'to_user_id'=>0,
                'client_ip'=>$_SERVER['REMOTE_ADDR'],
                'content'=>"终止拍卖",
                'price'=>$new_price,
                'num'=>$info['num'],
                'time'=>date('Y-m-d H:i:s'),
            );
            Db::name('activity_log')->insert($new_data);

            // 流拍推送
            $user_list = Db::name('activity_user')->alias('u')->field('u.user_id as id,u.name,u.tel,uu.openid')->leftJoin('user uu','u.user_id = uu.id')->where('u.activity_id',$info['id'])->select();
            $openid = Db::name('user')->where('id',$info['user_id'])->value('openid');
            sendTemplateLpWX($openid,$info['product_name'],date('Y-m-d H:i:s'),$info['title'],$info['activity_sn'],$user_list);
            return json(['code'=>'1','message'=>"操作成功！"]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 校验邀请码
    **/
    public function activityCheck(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $invitation_code = $this->request->param('invitation_code/d','');
            if($activity_sn==''||$invitation_code==''){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            try{
                $field = '*';
                $info = Db::name('activity')->field($field)->where(['activity_sn'=>$activity_sn])->find();
                if($info&&$info['invitation_code']==$invitation_code&&$info['is_open']=='0'){
                    $data = [];
                    $data['activity_id'] = $info['id'];
                    $data['user_id'] = $this->userInfo['id'];
                    $data['flag'] = 1;
                    $data['create_time'] = time();
                    Db::name('activity_user_check')->insert($data);
                    return json(['code'=>'1','message'=>"校验成功",'result'=>'']);
                }
                return json(['code'=>'0','message'=>"校验失败",'result'=>'']);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 获取间隔时间
    **/
    public function getTime(){
        $list = Db::name('time_config')->field('name,value')->order('sort desc,id asc')->select();
        return json(['code'=>'1','message'=>"成功",'result'=>$list]);
    }

    /**
     * 我参与的拍卖
    **/
    public function myJoin(){
        if ($this->request->isPost()) {
            try{
                $userInfo = $this->userInfo;
                $map[] = ['u.user_id','=', $userInfo['id']];
                $status = $this->request->param('status/d','');
                $keywords = $this->request->param('keywords/s','');
                if($status=='0'){
                    $map[] = ['u.status','=', 0];
                }
                if($status=='1'){
                    $map[] = ['u.flag','=',1];
                    $map[] = ['a.status','in', [0,1,4]];
                }
                if($status=='2'){
                    $map[] = ['u.flag','=',1];
                    $map[] = ['a.status','in', [2,3]];
                }
                if($keywords!=''){
                    $map[] = ['a.title','like','%'.$keywords.'%'];
                }

                $list = Db::name('activity_user')->alias('u')->field('a.activity_sn,a.title,a.reserve_price,a.cid,a.s_o_s,u.status,a.status as activity_status,a.view,a.start_time,a.end_time,a.type')->where($map)->join('activity a','u.activity_id = a.id','LEFT')->order('a.id', 'desc')->paginate(10);
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>$list]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 加入拍卖
    **/
    public function joinActivity(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $name = $this->request->param('name/s','');
            $tel = $this->request->param('tel/d','');
            $code = $this->request->param('code/d','');
            $corporate_name = $this->request->param('corporate_name/s','');
            if($activity_sn==''){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            if($name==''||$tel==''||$corporate_name==''){
                return json(['code'=>'0','message'=>"请完善信息"]);
            }
            $activityInfo = Db::name('activity')->where(['activity_sn'=>$activity_sn])->find();
            if(empty($activityInfo)){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $userInfo = $this->userInfo;
            if($activityInfo['user_id']==$userInfo['id']){
                return json(['code'=>'0','message'=>"自己创建的项目，不能参与拍卖"]);
            }
            $info = Db::name('activity_user')->where(['activity_id'=>$activityInfo['id'],'user_id'=>$userInfo['id']])->find();
            if(!empty($info)){
                return json(['code'=>'0','message'=>"尊敬的用户：您已经成功申请加入活动，请耐心等待主办方的审核。结果将会通过站内信和预留手机号短信形式通知。您还可以在“我的-我的申请列表”看到查看结果。"]);
            }
            try{
                if($tel!=$userInfo['tel']){
                    if(empty($code)){
                        return json(['code'=>'0','message'=>'请输入验证码']);
                    }
                    $this->checkSms($tel,$code,$userInfo['id']);
                }
                $characters = ['A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','T','U','V','W','X','Y'];
                $data['activity_id'] = $activityInfo['id'];
                $data['create_user_id'] = $activityInfo['user_id'];
                $data['user_id'] = $userInfo['id'];
                $data['status'] = 0;
                $data['flag'] = 0;
                $data['check_time'] = null;
                $data['corporate_name'] = $corporate_name;
                $data['name'] = $name;
                $data['from_client_name'] = $characters[rand(0, 21)].rand(1000,9999);
                $data['from_client_image'] = 'https://pm.e-stronger.com/user/images/avatar/user'.rand(1,897).'.jpg';
                $data['tel'] = $tel;
                $data['create_time'] = time();
                Db::name('activity_user')->insert($data);
                Db::name('activity')->where('id', $activityInfo['id'])->inc('view')->update();
                Db::name('user')->where('id', $userInfo['id'])->update(['tel'=>$tel,'name'=>$name,'corporate_name'=>$corporate_name]);
                $send_url='https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.access_token();
                $tmp_data=array();
                $tmp_data['touser']=Db::name('user')->where(['id'=>$activityInfo['user_id']])->value('openid');
                $tmp_data['template_id']='O3utiMM5svbwX_BO_uuGIVmFi04ufIBjrEF_Gho36Ps'; //拍场待审核通知
                $tmp_data['url']='https://pm.e-stronger.com/user/myApply.html';
                $tmp_data['data'] = [
                    "first"=>[
                        "value"=>"有新的拍申请等待您的审核",
                        "color"=>"#173177"
                    ],
                    "keyword1"=>[
                        "value"=>$activityInfo['title'],
                        "color"=>"#173177"
                    ],
                    "keyword2"=>[
                        "value"=>date('Y-m-d H:i:s',$activityInfo['start_time']),
                        "color"=>"#173177"
                    ],
                    "keyword3"=>[
                        "value"=>$name,
                        "color"=>"#173177"
                    ],
                    "remark"=>[
                        "value"=>"温馨提示：请尽量处理，以免用户错过拍卖。",
                        "color"=>"#ff0000"
                    ]
                ];
                $aaa = http_curl($send_url,json_encode($tmp_data));
                $new_message = array(
                    'type'=>'viewdata', 
                    'view'=>$activityInfo['view'],
                    'join_users_count'=>Db::name('activity_user')->where('activity_id',$activityInfo['id'])->count(),
                );
                Gateway::sendToGroup($activity_sn ,json_encode($new_message));
            }catch (\Exception $exception){
                return json(['code'=>'0','message'=>$exception->getMessage()]);
            }
            return json(['code'=>'1','message'=>"成功",'result'=>'']);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 审批列表
    **/
    public function checkList(){
        if ($this->request->isPost()) {
            $userInfo = $this->userInfo;
            $map[] = ['create_user_id','=', $userInfo['id']];
            $status = $this->request->param('status/d','0');
            $keywords = $this->request->param('keywords/s','');
            if($status==1){
                $map[] = ['au.status','=', 1];
            }else{
                $map[] = ['au.status','=', 0];
            }
            if($keywords!=''){
                $map[] = ['a.title|au.name|u.nickname','like','%'.$keywords.'%'];
            }
            $checkList = Db::name('activity_user')->alias('au')->field('au.id,au.name,u.nickname,from_unixtime(au.create_time,"%Y-%m-%d %H:%i:%s") AS time,au.flag,au.status,activity_sn,a.title,a.s_o_s')->where($map)->join('activity a','au.activity_id = a.id','LEFT')->join('user u','au.user_id = u.id','LEFT')->order('au.id')->paginate(10);
            return json(['code'=>'1','message'=>"成功",'result'=>$checkList]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 聊天记录
    **/
    public function chatRecord(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            if($activity_sn==''){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $activityInfo = Db::name('activity')->field('type,num,unit,id,user_id,activity_sn,status')->where(['activity_sn'=>$activity_sn])->find();
            if(empty($activityInfo)){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $userInfo = $this->userInfo;
            $self_data = NULL;
            if($activityInfo['user_id']==$userInfo['id']){
                // 拍卖场主
                $is_creator = true;
                $flag = true;
            }else{
                $is_creator = false;
                // 参与
                $info = Db::name('activity_user')->where(['activity_id'=>$activityInfo['id'],'user_id'=>$userInfo['id']])->find();
                if(!empty($info)&&$info['flag']==1){
                    $flag = true;
                }else{
                    $flag = false;
                }
            }
            $list = [];
            if($flag){
                if($is_creator){
                    if($activityInfo['status']==1||$activityInfo['status']==4){
                        if($activityInfo['type']==2||$activityInfo['type']==5||$activityInfo['type']==6||$activityInfo['type']==8){
                            $data = Db::name('activity_log')->alias('l')->field('l.from_client_id,l. from_client_name,l.from_client_image,l.content,l.time,l.price,l.num')->leftJoin('activity_user u','l.from_client_id = u.user_id')->where(['l.activity_sn'=>$activity_sn])->where(['u.activity_id'=>$activityInfo['id']])->where(['l.type'=>'offer'])->order('l.id desc')->select();
                        }else{
                            $data = Db::name('activity_log')->alias('l')->field('l.from_client_id,l. from_client_name,l.from_client_image,l.content,l.time,l.price,l.num')->leftJoin('activity_user u','l.from_client_id = u.user_id')->where(['l.activity_sn'=>$activity_sn])->where(['u.activity_id'=>$activityInfo['id']])->where(['l.type'=>'offer'])->order('l.price desc')->select();
                        }

                    }else{
                        if($activityInfo['type']==2||$activityInfo['type']==5||$activityInfo['type']==6||$activityInfo['type']==8){
                            $data = Db::name('activity_log')->alias('l')->field('l.from_client_id,u.name as from_client_name,l.from_client_image,l.content,l.time,l.price,l.num')->leftJoin('activity_user u','l.from_client_id = u.user_id')->where(['l.activity_sn'=>$activity_sn])->where(['u.activity_id'=>$activityInfo['id']])->where(['l.type'=>'offer'])->order('l.id desc')->select();
                        }elseif($activityInfo['type']==7){
                            $data = Db::name('activity_log')->alias('l')->field('l.from_client_id,u.name as from_client_name,l.from_client_image,l.content,l.time,l.price,l.num')->leftJoin('activity_user u','l.from_client_id = u.user_id')->where(['l.activity_sn'=>$activity_sn])->where(['u.activity_id'=>$activityInfo['id']])->where(['l.type'=>'offer'])->order('l.price asc')->select();
                        }else{
                            $data = Db::name('activity_log')->alias('l')->field('l.from_client_id,u.name as from_client_name,l.from_client_image,l.content,l.time,l.price,l.num')->leftJoin('activity_user u','l.from_client_id = u.user_id')->where(['l.activity_sn'=>$activity_sn])->where(['u.activity_id'=>$activityInfo['id']])->where(['l.type'=>'offer'])->order('l.price desc')->select();
                        }
                    }
                    foreach ($data as $k => $v){
                        $list[$k] = $v;
                        $list[$k]['is_self'] = false;
                        $list[$k]['content'] = nl2br($v['content']);
                    }
                    $self_data = NULL;
                }else{
                    if($activityInfo['type']==2||$activityInfo['type']==5||$activityInfo['type']==8){
                        $data = Db::name('activity_log')->field('from_client_id as is_self,from_client_name,from_client_image,content,time,price,num')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->order('id desc')->select();
                    }elseif($activityInfo['type']==6||$activityInfo['type']==7){
                        $data = Db::name('activity_log')->field('from_client_id as is_self,from_client_name,from_client_image,content,time,price,num')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->order('id desc')->select();
                    }else{
                        $data = Db::name('activity_log')->field('from_client_id as is_self,from_client_name,from_client_image,content,time,price,num')->where(['activity_sn'=>$activity_sn])->where(['type'=>'offer'])->order('price desc')->select();
                    }
                    foreach ($data as $k => $v){
                        $list[$k] = $v;
                        $list[$k]['is_self'] = $v['is_self']==$userInfo['id']?true:false;
                        $list[$k]['from_client_name'] = $v['is_self']==$userInfo['id']?$info['name']:$v['from_client_name'];
                        $list[$k]['content'] = nl2br($v['content']);
                    }
                    $self_data = Db::name('activity_log')->field('from_client_name,from_client_image,content,time,price')->where(['activity_sn'=>$activity_sn,'type'=>'offer','from_client_id'=>$userInfo['id']])->order('id desc')->find();
                    $self_data = $self_data?$self_data:NULL;
                    if($self_data){
                        $self_data['is_self'] = true;
                        $self_data['from_client_name'] = $info['name'];
                        $self_data['tel'] = $info['tel'];
                        if($list){
                            $price = $list[0]['price'];
                        }
                        $self_data['is_top'] = $price==$self_data['price']?true:false;
                    }
                }
            }
            return json(['code'=>'1','message'=>"成功",'result'=>['list'=>$list,'self_data'=>$self_data,'activity_info'=>$activityInfo]]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 提报记录
    **/
    public function pollingList(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $userInfo = $this->userInfo;
            $map = [];
            $map['l.type'] = 'offer';
            $map['l.activity_sn'] = $activity_sn;
            $activity_info = Db::name('activity')->where(['activity_sn'=>$activity_sn])->whereRaw('type=5 OR type=8')->find();
            $map['u.activity_id'] = $activity_info['id'];
            if(!$activity_info){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $data = [];
            $list = Db::name('activity_log')->alias('l')->field('l.from_client_image,l.from_client_id,u.name,l.from_client_name,l.price,l.time,l.num,l.content')->where($map)->leftJoin('activity_user u','l.from_client_id = u.user_id')->order('l.num decs,l.id desc')->select();
            foreach ($list as $k => $v){
                if($activity_info['user_id']==$userInfo['id']&&($activity_info['status']==2||$activity_info['status']==3)){
                    $v['from_client_name'] = $v['name'];
                }
                if($v['from_client_id']==$userInfo['id']){
                        $v['from_client_name'] = $v['name'];
                }else if($activity_info['user_id']!=$userInfo['id']&&($activity_info['status']==1||$activity_info['status']==4)){
                    continue;
                }

                $data[$v['num']]['title'] = "第".$v['num']."轮提报记录";
                if($activity_info['num']==$v['num']){
                    switch ($activity_info['status']) {
                        case 1:
                            $flag = '待确认';
                            break;
                        case 2:
                            $flag = '已中标';
                            break;
                        case 3:
                            $flag = '已作废';
                            break;
                        case 4:
                            $flag = '待确认';
                            break;
                        default:
                            break;
                    }
                }else{
                    $flag = '已作废';
                }
                $data[$v['num']]['flag'] = $flag;
                $data[$v['num']]['price'] = $v['content'];
                $data[$v['num']]['weight'] = isset($data[$v['num']]['weight'])?$data[$v['num']]['weight']+$v['price']:$v['price'];
                $data[$v['num']]['peoples'] = isset($data[$v['num']]['peoples'])?$data[$v['num']]['peoples']+1:1;
                $data[$v['num']]['child'][$k] = $v;
            }
            $data = array_reverse($data);
            return json(['code'=>'1','message'=>"成功",'result'=>$data]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 提报确认信息
    **/
    public function pollingInfo(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $userInfo = $this->userInfo;
            $activity_info = Db::name('activity')->where(['activity_sn'=>$activity_sn])->whereRaw('type=5 OR type=8')->find();
            if(!$activity_info){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            if($activity_info['user_id']!=$userInfo['id']){
                // 买方
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $map = [];
            $map['type'] = 'offer';
            $map['activity_sn'] = $activity_sn;
            $map['num'] = $activity_info['num'];
            $data = [];
            $data['weight'] = Db::name('activity_log')->where($map)->sum('price');
            $data['price'] = $activity_info['final_price'];
            $data['plan_weight'] = $activity_info['weight'];
            $data['end_time'] = date('Y-m-d H:i:s',time()+$activity_info['second']);
            return json(['code'=>'1','message'=>"成功",'result'=>$data]);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 多轮询提报确认操作
    **/
    public function pollingConfirm(){
        if ($this->request->isPost()) {
            $activity_sn = $this->request->param('activity_sn/s','');
            $end_time = $this->request->param('end_time/s','');
            $price = $this->request->param('price/d',0);
            $type = $this->request->param('type/d','');
            if(!$type||!$activity_sn){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $activity_info = Db::name('activity')->where(['activity_sn'=>$activity_sn])->whereRaw('type=5 OR type=8')->find();
            if(!$activity_info){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            if($activity_info['status']==2||$activity_info['status']==3){
                return json(['code'=>'0','message'=>"本场拍卖已经结束，请勿重复提报！"]);
            }
            $userInfo = $this->userInfo;
            if($activity_info['user_id']!=$userInfo['id']){
                // 买方
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            switch ($type) {
                case 1:
                    $flag = '确认';
                    $map = ['l.type' => 'offer','l.activity_sn' => $activity_sn,'l.num' => $activity_info['num'],'u.activity_id'=>$activity_info['id']];
                    $list = Db::name('activity_log')->alias('l')->field('u.user_id as id,uu.openid,u.name,u.tel,l.from_client_name,l.from_client_image,price as weight')->leftJoin('activity_user u','l.from_client_id = u.user_id')->leftJoin('user uu','l.from_client_id = uu.id')->where($map)->order('l.id desc')->select()->toArray();
                    if(count($list)){
                        $activity_status = 2;
                        $data = [];
                        foreach ($list as $k => $v){
                            $order_sn = 'H'.date('Ymd').rand_number(6);
                            $data[$k]['activity_sn'] = $activity_sn;
                            $data[$k]['order_sn'] = $order_sn;
                            $data[$k]['final_price'] = $activity_info['final_price'];
                            $data[$k]['weight'] = $v['weight'];
                            $data[$k]['unit'] = $activity_info['unit'];
                            $data[$k]['create_time'] = time();
                            $data[$k]['status'] = 0;
                            $data[$k]['user_id'] = $v['id'];
                            $data[$k]['c_status'] = 0;
                            $data[$k]['c_user_id'] = $userInfo['id'];
                            $list[$k]['order_sn'] = $order_sn;
                        }
                        Db::startTrans();
                        $update_falg = Db::name('activity')->where('id',$activity_info['id'])->update(['status'=>2,'update_time'=>time(),'winner'=>json_encode($list,JSON_UNESCAPED_UNICODE)]);
                        $insert_flag = Db::name('order')->insertAll($data);
                        if($update_falg&&$insert_flag){
                            Db::commit();
                            // 推送
                            sendTemplateWX($userInfo['openid'],$activity_info['title'],$activity_info['final_price'],$activity_info['tel'],$activity_info['activity_sn'],$list);
                        }else{
                            // 回滚事务
                            Db::rollback();
                            return json(['code'=>'0','message'=>"操作失败"]);
                            break;
                        }
                    }else{
                        $falg = Db::name('activity')->where('id',$activity_info['id'])->update(['status'=>3,'final_price'=>$price,'update_time'=>time()]);
                        $activity_status = 3;
                    }
                    $new_message = array(
                        'type'=>'info', 
                        'price'=>bcadd($price,0,2),
                        'my_price'=>bcadd($price,0,2),
                        'activity_status'=>$activity_status,
                        'activity_type'=>5,
                        'range'=>0,
                        'second'=>strtotime($end_time)-time(),
                        'num'=>$activity_info['num'],
                    );
                    Gateway::sendToGroup($activity_sn ,json_encode($new_message));
                    return json(['code'=>'1','message'=>"成功",'result'=>'']);
                    break;
                case 2:
                    // 下一轮
                    if(!$end_time){
                        return json(['code'=>'0','message'=>"请输入结束时间！"]);
                    }
                    if($price<=0){
                        return json(['code'=>'0','message'=>"请输入采购单价！"]);
                    }
                    $falg = Db::name('activity')->where(['id'=>$activity_info['id']])->update(['end_time'=>strtotime($end_time),'update_time'=>time(),'final_price'=>$price,'status'=>1,'num'=>$activity_info['num']+1]);
                    $new_message = array(
                        'type'=>'info', 
                        'price'=>bcadd($price,0,2),
                        'my_price'=>bcadd($price,0,2),
                        'activity_status'=>1,
                        'activity_type'=>5,
                        'range'=>0,
                        'second'=>strtotime($end_time)-time(),
                        'num'=>$activity_info['num']+1,
                        'weight'=>$activity_info['weight'],
                    );
                    Gateway::sendToGroup($activity_sn ,json_encode($new_message));
                    $send_url='https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.access_token();
                    $template_id = '3l6DXRgKOCPeg3KAn_1ODnwlu5EYnrSyXV_13QaV3tQ';
                    $tmp_data=array();
                    $tmp_data['template_id']=$template_id; //预约拍卖开始提醒
                    $tmp_data['url'] = 'https://pm.e-stronger.com/user/detail.html?activity_sn='.$activity_info['activity_sn'];
                    $tmp_data['data'] = [
                        "first"=>[
                            "value"=>"您好，您参与的拍卖已进入第".($activity_info['num']+1)."轮环节。",
                            "color"=>"#173177"
                        ],
                        "keyword1"=>[
                            "value"=>$activity_info['title'],
                            "color"=>"#173177"
                        ],
                        "keyword2"=>[
                            "value"=>date('Y-m-d H:i:s'),
                            "color"=>"#173177"
                        ],
                        "remark"=>[
                            "value"=>"温馨提示：请尽快入场重新提交意向。",
                            "color"=>"#ff0000"
                        ]
                    ];
                    $users = Db::name('activity_user')->alias('u')->field('u.tel,uu.openid')->leftJoin('user uu','u.user_id = uu.id')->where('u.activity_id',$activity_info['id'])->where('u.status',1)->where('u.flag',1)->select();
                    foreach ($users as $v){
                        sendTemplateAliSMS($v['tel'],['name'=>$activity_info['title'],'time'=>date('Y-m-d H:i:s')],'SMS_223195715',0);
                        // 买家
                        $tmp_data['touser']=$v['openid'];
                        http_curl($send_url,json_encode($tmp_data));
                    }

                    // 下一轮推送 
                    return json(['code'=>'1','message'=>"成功",'result'=>'']);
                    break;
                default:
                    return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
                    break;
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 审核
    **/
    public function check(){
        if ($this->request->isPost()) {
            $ids = $this->request->param('ids','[]');
            $flag = $this->request->param('flag/d',0);
            if(!($flag==1||$flag==2)){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            if(empty($ids)){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }
            $userInfo = $this->userInfo;
            $infos = Db::name('activity_user')->alias('au')
                ->field('au.id,au.name,au.tel,u.nickname,u.openid,from_unixtime(au.create_time,"%Y-%m-%d %H:%i:%s") AS time,au.flag,au.status,activity_sn,a.title,a.s_o_s,from_unixtime(a.start_time,"%Y-%m-%d %H:%i:%s") AS start_time')
                ->where('au.create_user_id',$userInfo['id'])
                ->where('au.id','in',$ids)
                ->join('activity a','au.activity_id = a.id','LEFT')
                ->join('user u','au.user_id = u.id','LEFT')
                ->select();
            if(empty($infos)){
                return json(['code'=>'0','message'=>"参数错误，请刷新页面！"]);
            }

            $data = [];
            $data['status'] = 1;
            $data['flag'] = $flag;
            $data['check_time'] = date('Y-m-d H:i:s');
            Db::name('activity_user')->where('id','in', $ids)->update($data);

            $send_url='https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.access_token();
            foreach ($infos as $vo) {
                $tmp_data=array();
                $tmp_data['touser']=$vo['openid'];
                $tmp_data['template_id']='7QJXWOXAS_0CsuC3TJCKRaNaAu4IyiGDXWt8wrOtK_M'; //信息审核通知
                $tmp_data['url']='https://pm.e-stronger.com/user/detail.html?activity_sn='.$vo['activity_sn'];
                if($flag==1){
                    //通过的推送
                    $tmp_data['data'] = [
                        "first"=>[
                            "value"=>"您好，".$vo['name'],
                            "color"=>"#173177"
                        ],
                        "keyword1"=>[
                            "value"=>$data['check_time'],
                            "color"=>"#173177"
                        ],
                        "keyword2"=>[
                            "value"=>"审核通过",
                            "color"=>"#173177"
                        ],
                        "remark"=>[
                            "value"=>"温馨提示：【".$vo['title']."】将在".$vo['start_time']."开始，请准时参加。",
                            "color"=>"#ff0000"
                        ]
                    ];
                    $this->sendTemplateSMS($vo['tel'],['name'=>$vo['title'],'time'=>$vo['start_time']],'SMS_223543479',0);
                }else{
                    //不通过的推送
                    $tmp_data['data'] = [
                        "first"=>[
                            "value"=>"您好，".$vo['name'],
                            "color"=>"#173177"
                        ],
                        "keyword1"=>[
                            "value"=>$data['check_time'],
                            "color"=>"#173177"
                        ],
                        "keyword2"=>[
                            "value"=>"审核不通过",
                            "color"=>"#ff0000"
                        ],
                        "remark"=>[
                            "value"=>"您申请参与【".$vo['title']."】的审核不通过，感谢您的参与。",
                            "color"=>"#173177"
                        ]
                    ];
                }
                http_curl($send_url,json_encode($tmp_data));
            }
            return json(['code'=>'1','message'=>"成功",'result'=>'']);
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    //校验短信验证码
    private function checkSms($tel,$code,$uid){
        if(check_mobile($tel)){
            $today=date("Ymd");
            if($code==''){
                throw new \Exception("请输入验证码");
            }
            $code_info=Db::name('sms_log')->where(array('uid'=>$uid,'tel'=>$tel,'adate'=>$today,'isuse'=>0))->order('id desc')->find();
            if(empty($code_info)){
                throw new \Exception("验证码不存在或已使用");
            }
            if($code_info['code']!=$code){
                throw new \Exception("验证码错误");
            }
            Db::name('sms_log')->where('id',$code_info['id'])->update(array('isuse'=>1));
        }else{
            throw new \Exception("手机号码错误，请重新输入");
        }
    }

    //获取短信验证码
    public function getSms(){
        if ($this->request->isPost()) {
            $tel = $this->request->param('tel/d',0);
            if(check_mobile($tel)){
                $userInfo = $this->userInfo;
                $time=time();
                $today=date("Ymd");
                $have=Db::name('sms_log')->where(array('uid'=>$userInfo['id'],'adate'=>$today))->count();
                if($have>10){
                    return json(['code'=>'0','message'=>"同一用户一天最多只能发送10条短信",'result'=>'']);
                }else{
                    $expire=60;
                    $last=Db::name('sms_log')->where('uid',$userInfo['id'])->order('id desc')->find();
                    $pass=$time-$last['atime'];
                    if($pass > $expire){
                        $code=rand_number(6);
                        $datas=array();
                        $datas['code']=$code;
                        $send_flag = $this->sendTemplateSMS($tel,$datas,'SMS_222760170',$userInfo['id'],1);
                        if($send_flag){
                            return json(['code'=>'1','message'=>'短信发送成功','result'=>'']);
                        }else{
                            return json(['code'=>'0','message'=>'短信发送失败','result'=>'']);
                        }
                    }else{
                        $later=$expire-$pass;
                        return json(['code'=>'0','message'=>'请'.$later.'秒后再获取','result'=>'']);
                    }
                }
            }else{
                return json(['code'=>'0','message'=>'手机号码格式错误','result'=>'']);
            }
        }else{
            return json(['code'=>'0','message'=>"请求类型错误,请用POST方式请求"]);
        }
    }

    /**
     * 发送模板短信
     * @param to 手机号码集合,用英文逗号分开
     * @param datas 内容数据 格式为数组 例如：array('Marry','Alon')，如不需替换请填 null
     * @param $tempId 模板Id,测试应用和未上线应用使用测试模板请填写1，正式应用上线后填写已申请审核通过的模板ID
     */
    private function sendTemplateSMS($to,$datas,$tempId,$uid,$type=1,$temp=false){
        AlibabaCloud::accessKeyClient('LTAI5t8ePok7jueLjsGfwTFq', 'Z1S3XvtXhTnmN4aP4hGNN4xzrxVOV2')
            ->regionId('cn-shenzhen')
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'PhoneNumbers' => $to,
                        'SignName' => "智油网",
                        'TemplateParam' => json_encode($datas),
                        'TemplateCode' => $tempId,
                    ],
                ])
                ->request();
            if($temp){
                return true;
            }

            $result = $result->toArray();
            if($result['Code']!='OK') {
                return false;
            }else{
                $time=time();
                $today=date("Ymd");
                $data=array();
                $data['uid']=$uid;
                $data['tel']=$to;
                $data['code']=isset($datas['code'])?$datas['code']:'';
                $data['info']=json_encode($datas);
                $data['atime']=$time;
                $data['type']=$type;
                $data['adate']=$today;
                if(Db::name('sms_log')->insert($data)){
                    return true;
                }else{
                    return false;
                }
            }
        } catch (ClientException $e) {
            return false;
        } catch (ServerException $e) {
            return false;
        }
    }
}